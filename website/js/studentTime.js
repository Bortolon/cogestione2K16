var apikey = "";
var request = true;
var logout = function() {
    document.querySelector(".information").innerHTML = "Attendi";
    document.querySelector(".information").className = "information positive";
    var xhr = new XMLHttpRequest();//richiesta dati
    xhr.open("DELETE", "/api/v1/login?apikey="+apikey, true);
    xhr.onload = function (e) {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                var result = JSON.parse(xhr.responseText, function (key, value) {
                    var type;
                    if (value && typeof value === 'object') {
                        type = value.type;
                        if (typeof type === 'string' && typeof window[type] === 'function') {
                            return new (window[type])(value);
                        }
                    }
                    return value;
                });
                if(result.message == "OK")
                {
                    location.href = "/index.html";
                }
                else
                {
                    document.querySelector(".information").innerHTML = result.message;
                    document.querySelector(".information").className = "information error";
                    location.href = "/index.html";
                    request = false;
                }
                console.log(xhr.responseText);
            } else {
                document.querySelector(".information").innerHTML = "Errore del server, riprova più tardi";
                document.querySelector(".information").className = "information error";
                location.href = "/index.html";
                console.error(xhr.statusText);
                request = false;
            }
        }
    };
    xhr.onerror = function (e) {
        document.querySelector(".information").innerHTML = "Errore nella richiesta, riprova!!!";
        document.querySelector(".information").className = "information error";
        location.href = "/index.html";
    };
    xhr.send();
}
window.onload = function() {
    document.querySelector(".information").innerHTML = "Attendi";
    document.querySelector(".information").className = "information positive";
    apikey = location.href.split("?")[1].split("=")[1];
    var xhr = new XMLHttpRequest();//richiesta dati
    xhr.open("GET", "/api/v1/course?apikey="+apikey, true);
    xhr.onload = function (e) {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                var result = JSON.parse(xhr.responseText, function (key, value) {
                    var type;
                    if (value && typeof value === 'object') {
                        type = value.type;
                        if (typeof type === 'string' && typeof window[type] === 'function') {
                            return new (window[type])(value);
                        }
                    }
                    return value;
                });
                if(result.message == "OK")
                {
                    var printLine = "";
                    printLine += "<table class=\"table\"><thead><tr><th>Data inizio:</th><th>Durata:</th><th>Laboratorio:</th><th>Aula:</th></tr></thead><tbody>";
                    for(var p=0; p < result.orari.length; p++)
                    {
                        var dataOra = new Date(result.orari[p].oraInizio.replace(" ","T"));
                        printLine += "<tr><td>"+dataOra.toLocaleDateString()+" "+dataOra.getHours()+":"+dataOra.getMinutes()+"</td><td>"+result.orari[p].durata+"</td><td>"+result.orari[p].nomeLaboratorio+"</td><td>"+result.orari[p].aula+"</td></tr>";
                    }
                    printLine += "</tbody></table>";
                    document.querySelector(".time-table").innerHTML = printLine;
                    document.querySelector(".information").innerHTML = "";
                    document.querySelector(".information").className = "information";
                    request = false;
                }
                else if(result.message == "Laboratori not insert for this user")
                {
                    document.querySelector(".information").innerHTML = "Laboratori non inseriti";
                    document.querySelector(".information").className = "information error";
                    logout();
                    request = false;
                }
                else if(result.message == "No hours in the system")
                {
                    document.querySelector(".information").innerHTML = "Non ci sono ore nel sistema";
                    document.querySelector(".information").className = "information error";
                    logout();
                    request = false;
                }
                else if(result.message == "Api Key is not valid")
                {
                    document.querySelector(".information").innerHTML = "Login errato";
                    document.querySelector(".information").className = "information error";
                    logout();
                    request = false;
                }
                else
                {
                    document.querySelector(".information").innerHTML = result.message;
                    document.querySelector(".information").className = "information error";
                    logout();
                    request = false;
                }
                console.log(xhr.responseText);
            } else {
                document.querySelector(".information").innerHTML = "Errore del server, riprova più tardi";
                document.querySelector(".information").className = "information error";
                logout();
                console.error(xhr.statusText);
                request = false;
            }
        }
    };
    xhr.onerror = function (e) {
        document.querySelector(".information").innerHTML = "Errore nella richiesta, riprova!!!";
        document.querySelector(".information").className = "information error";
        logout();
    };
    xhr.send();
    document.querySelector(".btn-logout").onclick = logout;
}