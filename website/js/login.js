var request = false;
document.getElementById("loginForm").addEventListener("submit", function(e){
    e.preventDefault();
    if(request==false)
    {
        if(document.getElementsByName("username")[0].value != "" && document.getElementsByName("password")[0].value != "")
        {
            var xhr = new XMLHttpRequest();
            var param = "username=" + document.getElementsByName("username")[0].value + "&password=" + document.getElementsByName("password")[0].value;
            xhr.open("POST", "/api/v1/login", true);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.onload = function (e) {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                            var result = JSON.parse(xhr.responseText, function (key, value) {
                                var type;
                                if (value && typeof value === 'object') {
                                    type = value.type;
                                    if (typeof type === 'string' && typeof window[type] === 'function') {
                                        return new (window[type])(value);
                                    }
                                }
                                return value;
                            });
                            if(result.message == "OK")
                            {
                                document.querySelector(".information").innerHTML = "Attendi...";
                                document.querySelector(".information").className = "information positive";
                                window.location.href = result.redirectAddress;
                            }
                            else if(result.message == "User not found")
                            {
                                document.querySelector(".information").innerHTML = "Utente non trovato";
                                document.querySelector(".information").className = "information error";
                                document.getElementsByName("password")[0].value = "";
                                request = false;
                            }
                            else
                            {
                                document.querySelector(".information").innerHTML = result.message;
                                document.querySelector(".information").className = "information error";
                                document.getElementsByName("password")[0].value = "";
                                request = false;
                            }
                            console.log(xhr.responseText);
                    } else {
                        document.querySelector(".information").innerHTML = "Errore del server, riprova pi&ugrave; tardi";
                        document.querySelector(".information").className = "information error";
                        document.getElementsByName("password")[0].value = "";
                        console.error(xhr.statusText);
                        request = false;
                    }
                    if(request == false)
                    {
                        document.querySelector(".btn-disable").className = "float btn";
                        document.querySelector(".btn").disabled = false;
                    }
                }
            };
            xhr.onerror = function (e) {
                document.querySelector(".information").innerHTML = "Errore nella richiesta, riprova!!!";
                document.querySelector(".information").className = "information error";
                document.getElementsByName("password")[0].value = "";
                console.error(xhr.statusText);
                document.querySelector(".btn-disable").className = "float btn";
                document.querySelector(".btn").disabled = false;
            };
            xhr.send(param);
            request = true;
            document.querySelector(".btn").className = "float btn-disable";
            document.querySelector(".btn-disable").disabled = true;
        }
    }
});