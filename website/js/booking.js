var apikey = "";
var request = true;
var orariCol = new Array();
var printName = new Array();
var nameShow = new Array();
var logout = function() {
    document.querySelector(".send-btn").disabled = true;
    document.querySelector(".send-btn").className = "float send-btn btn-disable";
    document.querySelector(".information").innerHTML = "Attendi";
    document.querySelector(".information").className = "information positive";
    var xhr = new XMLHttpRequest();//richiesta dati
    xhr.open("DELETE", "/api/v1/login?apikey="+apikey, true);
    xhr.onload = function (e) {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                var result = JSON.parse(xhr.responseText, function (key, value) {
                    var type;
                    if (value && typeof value === 'object') {
                        type = value.type;
                        if (typeof type === 'string' && typeof window[type] === 'function') {
                            return new (window[type])(value);
                        }
                    }
                    return value;
                });
                if(result.message == "OK")
                {
                    location.href = "/index.html";
                }
                else
                {
                    document.querySelector(".information").innerHTML = result.message;
                    document.querySelector(".information").className = "information error";
                    location.href = "/index.html";
                    request = false;
                }
                console.log(xhr.responseText);
            } else {
                document.querySelector(".information").innerHTML = "Errore del server, riprova più tardi";
                document.querySelector(".information").className = "information error";
                location.href = "/index.html";
                console.error(xhr.statusText);
                request = false;
            }
        }
    };
    xhr.onerror = function (e) {
        document.querySelector(".information").innerHTML = "Errore nella richiesta, riprova!!!";
        document.querySelector(".information").className = "information error";
        location.href = "/index.html";
    };
    xhr.send();
}
var overlay = function() {
    var data = document.querySelector(".helpMessage");
    if(data!=null)
    {
        if(data.classList.contains("hidden"))
        {
            data.classList.remove("hidden");
        }
        else
        {
            data.classList.add("hidden");
        }
    }
}
function disableSelection(target){

    if (typeof target.onselectstart!="undefined") //IE route
        target.onselectstart=function(){return false}

    else if (typeof target.style.MozUserSelect!="undefined") //Firefox route
        target.style.MozUserSelect="none"

    else //All other route (ie: Opera)
        target.onmousedown=function(){return false}

    target.style.cursor = "default"
}
var checkChange = function() {
    var elenco = this.name.split("-");
    var numeroInizioSelect = -1;
    var numeroFineSelect = -1;
    for(var d = 0; d < orariCol.length; d++)
    {
        if(orariCol[d]==parseInt(elenco[0]))
        {
            numeroInizioSelect = d;
            numeroFineSelect = d + parseInt(elenco[2]) - 1;
        }
    }
    if(numeroInizioSelect!=-1)
    {
        for(var c = 0; c < printName.length; c++)
        {
            if(document.querySelector("input[name=\""+printName[c]+"\"]").checked&&printName[c]!=this.name)
            {
                var splitting = printName[c].split("-");
                if(elenco[1]==splitting[1])
                {
                    document.querySelector("input[name=\""+printName[c]+"\"]").checked = false;
                }
                var numeroInizio = -1;
                var numeroFine = -1;
                for(var d = 0; d < orariCol.length; d++)
                {
                    if(orariCol[d]==parseInt(splitting[0]))
                    {
                        numeroInizio = d;
                        numeroFine = d + parseInt(splitting[2]) - 1;
                    }
                }
                if(numeroInizio!=-1)
                {
                    if((numeroInizioSelect>=numeroInizio&&numeroInizioSelect<=numeroFine)||(numeroFineSelect>=numeroInizio&&numeroFineSelect<=numeroFine))
                    {
                        document.querySelector("input[name=\""+printName[c]+"\"]").checked = false;
                    }
                }
                else
                {
                    console.log("error: unknown data");
                }
            }
        }
    }
    else
    {
        console.log("error: unknown data");
    }
}
var information = function() {
    var data = document.querySelector(".HD"+this.name);
    if(data!=null)
    {
        if(data.classList.contains("hidden"))
        {
            for(var p = 0; p < nameShow.length; p++)
            {
                document.querySelector(".HD"+nameShow[p]).classList.add("hidden");
                document.querySelector("a[name=\""+nameShow[p]+"\"]").innerHTML = "<span class=\"fa fa-plus\"></span>";
            }
            data.className = "HD"+this.name;
            document.querySelector("a[name=\""+this.name+"\"]").innerHTML = "<span class=\"fa fa-minus\"></span>";
        }
        else
        {
            data.className = "HD"+this.name+" hidden";
            document.querySelector("a[name=\""+this.name+"\"]").innerHTML = "<span class=\"fa fa-plus\"></span>";
        }
    }
}
var popolateTable = function() {
    var xhr = new XMLHttpRequest();//richiesta dati
    xhr.open("GET", "/api/v1/course?apikey="+apikey, true);
    xhr.onload = function (e) {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                var result = JSON.parse(xhr.responseText, function (key, value) {
                    var type;
                    if (value && typeof value === 'object') {
                        type = value.type;
                        if (typeof type === 'string' && typeof window[type] === 'function') {
                            return new (window[type])(value);
                        }
                    }
                    return value;
                });
                if(result.message == "OK")
                {
                    var printString = "<table class=\"table\"><thead><th>Laboratorio:</th>";
                    var count = 0;
                    for(var a = 0; a < result.orari.length; a++) {
                        orariCol[count] = result.orari[a].idorari;
                        var dataOra = new Date(result.orari[a].orarioinizio.replace(" ","T"));
                        printString += "<th>"+dataOra.toLocaleDateString()+" "+dataOra.getHours()+":"+dataOra.getMinutes()+"</th>";
                        count++;
                    }
                    printString += "</thead>";
                    for(var a = 0; a < result.laboratori.length; a++) {
                        printString += "<tr><td><a class=\"plus-icon\" name=\""+result.laboratori[a].idlaboratorio+"\"><span class=\"fa fa-plus\"></span></a>"+result.laboratori[a].nomeLaboratorio+"<div class=\"hidden HD"+result.laboratori[a].idlaboratorio+"\">"+result.laboratori[a].descrizione+"<br><span class=\"intestation\">Durata:</span> "+result.laboratori[a].durata+" turni</div></td>";
                        var id = result.laboratori[a].idlaboratorio;
                        var orarioStampa = new Array();
                        nameShow.push(""+result.laboratori[a].idlaboratorio);
                        for(var e = 0; e < result.laboratori[a].orariQuestoLab.length; e++) {
                            var foundCol = 0;
                            for(var p = 0; p < orariCol.length; p++)
                            {
                                if(orariCol[p]==result.laboratori[a].orariQuestoLab[e].idOrario)
                                {
                                    foundCol = p;
                                }
                            }
                            orarioStampa[foundCol] = result.laboratori[a].orariQuestoLab[e];
                        }
                        for(var p = 0; p < orariCol.length; p++) {
                            if(orarioStampa[p]!=null)
                            {
                                var data = "";
                                var stop = "";
                                if(orarioStampa[p].mancanti > 15)
                                {
                                    data = "booking-green";
                                }
                                else if(orarioStampa[p].mancanti <= 15 && orarioStampa[p].mancanti > 5)
                                {
                                    data = "booking-yellow";
                                }
                                else
                                {
                                    data = "booking-red";
                                }
                                if(orarioStampa[p].mancanti<=0)
                                {
                                    stop = " disabled readonly";
                                }
                                printString += "<td colspan=\""+result.laboratori[a].durata+"\"><input type=\"checkbox\" name=\""+orariCol[p]+"-"+result.laboratori[a].idlaboratorio+"-"+result.laboratori[a].durata+"\""+stop+"><span class=\"mancanti "+data+"\">"+orarioStampa[p].mancanti+"</span></td>";
                                printName.push(orariCol[p]+"-"+result.laboratori[a].idlaboratorio+"-"+result.laboratori[a].durata);
                                p += result.laboratori[a].durata - 1;
                            }
                            else
                            {
                                printString += "<td></td>";
                            }
                        }
                        count++;
                        printString += "</tr>";
                    }
                    printString += "</table>";
                    document.querySelector(".booking-table").innerHTML = printString;
                    for(var d = 0; d < printName.length; d++)
                    {
                        var splitting = printName[d].split("-");
                        if(result.scelte!=null)
                        {
                            for(var k = 0; k < result.scelte.length; k++)
                            {
                                if(result.scelte[k].idOrario==parseInt(splitting[0])&&result.scelte[k].idLaboratorio==parseInt(splitting[1]))
                                {
                                     document.querySelector("input[name=\""+printName[d]+"\"]").checked = true;
                                }
                            }
                        }
                        document.querySelector("input[name=\""+printName[d]+"\"]").onchange = checkChange;    
                    }
                    for(var d = 0; d < nameShow.length; d++)
                    {
                        document.querySelector("a[name=\""+nameShow[d]+"\"]").onclick = information;
                    }
                    document.querySelector(".send-btn").disabled = false;
                    document.querySelector(".send-btn").className = "float send-btn btn";
                    document.querySelector(".information").innerHTML = "";
                    document.querySelector(".information").className = "information";
                    request = false;
                }
                else if(result.message == "Laboratori not insert for this user")
                {
                    document.querySelector(".information").innerHTML = "Laboratori non inseriti";
                    document.querySelector(".information").className = "information error";
                    logout();
                    request = false;
                }
                else if(result.message == "No hours in the system")
                {
                    document.querySelector(".information").innerHTML = "Non ci sono ore nel sistema";
                    document.querySelector(".information").className = "information error";
                    logout();
                    request = false;
                }
                else if(result.message == "Api Key is not valid")
                {
                    document.querySelector(".information").innerHTML = "Login errato";
                    document.querySelector(".information").className = "information error";
                    logout();
                    request = false;
                }
                else
                {
                    document.querySelector(".information").innerHTML = result.message;
                    document.querySelector(".information").className = "information error";
                    logout();
                    request = false;
                }
                console.log(xhr.responseText);
            } else {
                document.querySelector(".information").innerHTML = "Errore del server, riprova più tardi";
                document.querySelector(".information").className = "information error";
                logout();
                console.error(xhr.statusText);
                request = false;
            }
        }
    };
    xhr.onerror = function (e) {
        document.querySelector(".information").innerHTML = "Errore nella richiesta, riprova!!!";
        document.querySelector(".information").className = "information error";
        logout();
    };
    xhr.send();
}
window.onload = function() {
    overlay();
    document.querySelector(".send-btn").disabled = true;
    document.querySelector(".send-btn").className = "float send-btn btn-disable";
    document.querySelector(".information").innerHTML = "Attendi";
    document.querySelector(".information").className = "information positive";
    apikey = location.href.split("?")[1].split("=")[1];
    popolateTable();
    document.querySelector(".btn-logout").onclick = logout;
    disableSelection(document.body);
}
document.getElementById("bookingForm").addEventListener("submit", function(e){
    e.preventDefault();
    if(request==false)
    {
        document.querySelector(".send-btn").disabled = true;
        document.querySelector(".send-btn").className = "float send-btn btn-disable";
        document.querySelector(".information").innerHTML = "Attendi";
        document.querySelector(".information").className = "information positive";
        var bookings = new Array();
        var valid = true;
        var bookingMade = new Array();
        for(var p=0;p<printName.length;p++)
        {
            var checkBox = document.querySelector("input[name=\""+printName[p]+"\"]");
            if(checkBox.checked)
            {
                var splitting = printName[p].split("-");
                var found = -1;
                for(var d = 0; d < orariCol.length; d++)
                {
                    if(orariCol[d]==parseInt(splitting[0]))
                    {
                        found = d;
                    }
                }
                for(var c=found;c < found + parseInt(splitting[2]); c++)
                {
                    bookingMade[c] = true;
                }
                bookings.push(printName[p]);
            };
        }
        if(bookingMade.length==orariCol.length)
        {
            for(var p=0; p<bookingMade.length;p++)
            {
                if(bookingMade[p]==null)
                {
                    valid = false;
                };
            }
        }
        else
        {
            valid = false;
        }
        if(valid)
        {
            var xhr = new XMLHttpRequest();
            var arrayLaboratori = "";
            var arrayFasce = "";
            for(var c=0;c<bookings.length;c++)
            {
                var splitting = bookings[c].split("-");
                arrayLaboratori += "arrayLaboratori[]=" + splitting[1] + "&";
                arrayFasce += "arrayFasce[]=" + splitting[0] + "&";
            }
            arrayFasce = arrayFasce.substring(0,arrayFasce.length-1);
            var param = arrayLaboratori + arrayFasce;
            xhr.open("POST", "/api/v1/booking?apikey="+apikey, true);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.onload = function (e) {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                            var result = JSON.parse(xhr.responseText, function (key, value) {
                                var type;
                                if (value && typeof value === 'object') {
                                    type = value.type;
                                    if (typeof type === 'string' && typeof window[type] === 'function') {
                                        return new (window[type])(value);
                                    }
                                }
                                return value;
                            });
                            if(result.message == "OK")
                            {
                                document.querySelector(".information").innerHTML = "Dati inviati";
                                document.querySelector(".information").className = "information positive";
                                logout();
                            }
                            else if(result.message == "Problematic Case")
                            {
                                document.querySelector(".information").innerHTML = "Laboratori non più disponibili";
                                document.querySelector(".information").className = "information";
                                popolateTable();
                                for(var c=0;c<bookings.length;c++)
                                {
                                    var splitting = bookings[c].split("-");
                                    arrayLaboratori += "arrayLaboratori[]=" + splitting[1] + "&";
                                    arrayFasce += "arrayFasce[]=" + splitting[0] + "&";
                                }
                                for(var c = 0; c < result.problematic.length; c++)
                                {
                                    document.querySelector()
                                }
                                for(var d = 0; d < result.problematic.length; d++)
                                {
                                    document.querySelector("input[name=\""+result.problematic[d].idOra+"-"+result.problematic[d].idLaboratorio+"\"]").checked = false;
                                }
                            }
                            else if(result.message == "Require parameter is not set")
                            {
                                document.querySelector(".information").innerHTML = "I parametri richiesti non sono stati settatt";
                                document.querySelector(".information").className = "information error";
                                request = false;
                            }
                            else if(result.message == "Invalid pass parameter data")
                            {
                                document.querySelector(".information").innerHTML = "Parametri non corretti";
                                document.querySelector(".information").className = "information error";
                                request = false;
                            }
                            else if(result.message == "Forbidden operation for this user level")
                            {
                                document.querySelector(".information").innerHTML = "Utente non abilitato all'esecuzione della specifica operazione";
                                document.querySelector(".information").className = "information error";
                                setTimeout(function(){
                                    logout();
                                }, 1500);
                            }
                            else if(result.message == "Api Key is not valid")
                            {
                                document.querySelector(".information").innerHTML = "Utente non trovato";
                                document.querySelector(".information").className = "information error";
                                setTimeout(function(){
                                    logout();
                                }, 1500);
                            }
                            else
                            {
                                document.querySelector(".information").innerHTML = result.message;
                                document.querySelector(".information").className = "information error";
                                request = false;
                            }
                            console.log(xhr.responseText);
                    } else {
                        document.querySelector(".information").innerHTML = "Errore del server, riprova più tardi";
                        document.querySelector(".information").className = "information error";
                        console.error(xhr.statusText);
                        request = false;
                    }
                    if(request == false)
                    {
                        document.querySelector(".send-btn").disabled = false;
                        document.querySelector(".send-btn").className = "float send-btn btn";
                    }
                }
            };
            xhr.onerror = function (e) {
                document.querySelector(".information").innerHTML = "Errore nella richiesta, riprova!!!";
                document.querySelector(".information").className = "information error";
                console.error(xhr.statusText);
                document.querySelector(".send-btn").disabled = false;
                document.querySelector(".send-btn").className = "float send-btn btn";
            };
            xhr.send(param);
            request = true;
            document.querySelector(".btn").className = "float btn-disable";
            document.querySelector(".btn-disable").disabled = true;
        }
        else
        {
            scroll(0,0);
            document.querySelector(".information").innerHTML = "&Egrave; ammesso un solo laboratorio per turno, e i laboratori non pu&ograve; essere ripetuto";
            document.querySelector(".information").className = "information error";
            document.querySelector(".send-btn").disabled = false;
            document.querySelector(".send-btn").className = "float send-btn btn";
        }
    }
});