var apikey = "";
var request = true;
var ModStatus = "notSelect";
var debugSettings = {//This group of function enable some function that help developer during debug as the disable of debug or the output of the webserver request
    "forceLogout" : true,
    "disableForceLogout" : function() {
        debugSettings.forceLogout = false;
    },
    "requestOutput" : false,
    "enableRequestOutput" : function() {
        debugSettings.requestOutput = true;
    }
};
var logout = function() {//This function execute the logout from the program set all the show parameter to the monitor and made a request to delete the apikey in both case of request succesfull or deny the resend to the home page
    document.querySelector(".information").innerHTML = "Attendi";
    document.querySelector(".information").className = "information positive";
    var xhr = new XMLHttpRequest();//richiesta dati
    xhr.open("DELETE", "/api/v1/login?apikey="+apikey, true);
    xhr.onload = function (e) {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                var result = JSON.parse(xhr.responseText, function (key, value) {
                    var type;
                    if (value && typeof value === 'object') {
                        type = value.type;
                        if (typeof type === 'string' && typeof window[type] === 'function') {
                            return new (window[type])(value);
                        }
                    }
                    return value;
                });
                if(result.message == "OK")
                {
                    location.href = "/index.html";
                }
                else
                {
                    document.querySelector(".information").innerHTML = result.message;
                    document.querySelector(".information").className = "information error";
                    location.href = "/index.html";
                    request = false;
                }
                if(debugSettings.requestOutput) { console.log(xhr.responseText); }
            } else {
                document.querySelector(".information").innerHTML = "Errore del server, riprova più tardi";
                document.querySelector(".information").className = "information error";
                location.href = "/index.html";
                console.error(xhr.statusText);
                request = false;
            }
        }
    };
    xhr.onerror = function (e) {
        document.querySelector(".information").innerHTML = "Errore nella richiesta, riprova!!!";
        document.querySelector(".information").className = "information error";
        location.href = "/index.html";
    };
    xhr.send();
};
var laboratorio = {//This contain all the function to show and manage the lab list
    elencoOrari : null,//This contain the hour obtain from the first request
    elencoClassi : null,//This contain the list of all class obtain from the first request
    elencoLaboratori : new Array(),//This contain the list of lab obtain from the first request and after refresh at every add edit remove
    hourName : new Array(),//This mantain the list of the input name of the hour checkbox
    checkName : new Array(),//this mantain the list of the input checkbox
    checkClassi : new Array(),
    checkCategoria : new Array(),
    show : function() {
        changeNavPosition("laboratori-nav");
        document.querySelector(".information").innerHTML = "Attendi";
        document.querySelector(".information").className = "information positive";
        document.querySelector(".show-navigation-bar").innerHTML = "";
        var xhr = new XMLHttpRequest();//richiesta dati
        xhr.open("GET", "/api/v1/course?apikey="+apikey, true);
        xhr.onload = function (e) {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    var result = JSON.parse(xhr.responseText, function (key, value) {
                        var type;
                        if (value && typeof value === 'object') {
                            type = value.type;
                            if (typeof type === 'string' && typeof window[type] === 'function') {
                                return new (window[type])(value);
                            }
                        }
                        return value;
                    });
                    if(result.message == "OK") {
                        document.querySelector(".show-navigation-bar").innerHTML = "<p class=\"intestation\">Elenco Laboratori:</p><div class=\"LaboratoriList\"></div><p class=\"intestation\">Dettagli Laboratorio:</p><div class=\"laboratorio-detail\">Seleziona un laboratorio per poterlo modificare!!!</div>";
                        laboratorio.elencoOrari = result.orari;
                        laboratorio.elencoClassi = result.classi;
                        var println = "";
                        for(var p=0;p<result.laboratori.length;p++) {
                            laboratorio.elencoLaboratori.push("LAB"+result.laboratori[p].idlaboratorio);
                            println += "<button class=\"laboratorioItem\" name=\"LAB"+result.laboratori[p].idlaboratorio+"\">" + result.laboratori[p].nomeLaboratorio+"</button>";
                        }
                        println += "<button class=\"laboratorioItem\" name=\"LABaddlaboratorio\"><span class=\"fa fa-plus plus-icon\"></span>Aggiungi laboratorio</button>";
                        laboratorio.elencoLaboratori.push("LABaddlaboratorio");
                        document.querySelector(".LaboratoriList").innerHTML = println;
                        for(var p=0;p<laboratorio.elencoLaboratori.length;p++) {
                            document.querySelector("button[name=\""+laboratorio.elencoLaboratori[p]+"\"]").onclick = laboratorio.changeLab;
                        }
                        document.querySelector(".information").innerHTML = "";
                        document.querySelector(".information").className = "information";
                        request = false;
                    }
                    else if(result.message == "No hours in the system") {
                        document.querySelector(".information").innerHTML = "Non ci sono ore nel sistema";
                        document.querySelector(".information").className = "information error";
                        request = false;
                    }
                    else if(result.message == "Api Key is not valid")
                    {
                        document.querySelector(".information").innerHTML = "Login errato";
                        document.querySelector(".information").className = "information error";
                        if(debugSettings.forceLogout) { logout(); }
                        request = false;
                    }
                    else if(result.message == "Classi not insert in the system") {
                        document.querySelector(".information").innerHTML = "Classi non inserite nel sistema";
                        document.querySelector(".information").className = "information error";
                        request = false;
                    }
                    else
                    {
                        document.querySelector(".information").innerHTML = result.message;
                        document.querySelector(".information").className = "information error";
                        if(debugSettings.forceLogout) { logout(); }
                        request = false;
                    }
                    if(debugSettings.requestOutput) { console.log(xhr.responseText); }
                } else {
                    document.querySelector(".information").innerHTML = "Errore del server, riprova più tardi";
                    document.querySelector(".information").className = "information error";
                    if(debugSettings.forceLogout) { logout(); }
                    request = false;
                }
            }
        };
        xhr.onerror = function (e) {
            document.querySelector(".information").innerHTML = "Errore nella richiesta, riprova!!!";
            document.querySelector(".information").className = "information error";
            if(debugSettings.forceLogout) { logout(); }
        };
        xhr.send();
    },
    checkHourPoint : function() {
        var oriDurata = parseInt(document.querySelector("input[name=\"durationLab\"]").value);//this obtain the duration of the course
        var toDeactivate = 0;//this is used to remember the number of next turn to disable
        for(var p=0;p<laboratorio.elencoOrari.length;p++)//We pass all the turn
        {
            if(toDeactivate!=0) {//If we have turn of more than one we disable the next collums of class and hour
                document.querySelector("input[name=\"ORA"+laboratorio.elencoOrari[p].idorari+"\"]").disabled = true;
                document.querySelector("input[name=\"ORA"+laboratorio.elencoOrari[p].idorari+"\"]").readonly = true;
                document.querySelector("input[name=\"ORA"+laboratorio.elencoOrari[p].idorari+"\"]").checked = false;
                toDeactivate--;
                laboratorio.changeClass(laboratorio.elencoOrari[p].idorari, true);
            }
            else if(p>laboratorio.elencoOrari.length-oriDurata) {//this else deny the possibility if the course during more than one turn to enable the latest turn (with the procedure of the last usable turn)
                document.querySelector("input[name=\"ORA"+laboratorio.elencoOrari[p].idorari+"\"]").disabled = true;
                document.querySelector("input[name=\"ORA"+laboratorio.elencoOrari[p].idorari+"\"]").readonly = true;
                document.querySelector("input[name=\"ORA"+laboratorio.elencoOrari[p].idorari+"\"]").checked = false;
                laboratorio.changeClass(laboratorio.elencoOrari[p].idorari, true);
            }
            else if(document.querySelector("input[name=\"ORA"+laboratorio.elencoOrari[p].idorari+"\"]").checked==true)//If the current hour cell is enable we can enable all the class below 
            {
                toDeactivate = oriDurata-1;
                laboratorio.changeClass(laboratorio.elencoOrari[p].idorari, false);
            }
            else//else we permit to select the hour cell but not the class cell below
            {
                document.querySelector("input[name=\"ORA"+laboratorio.elencoOrari[p].idorari+"\"]").disabled = false;
                document.querySelector("input[name=\"ORA"+laboratorio.elencoOrari[p].idorari+"\"]").readonly = false;
                laboratorio.changeClass(laboratorio.elencoOrari[p].idorari, true);
            }
        }
    },
    changeClass : function(idOrari, status) {//this with a cycle disable all the cell below a certain hour id
        for(var cre=0;cre<laboratorio.checkName.length;cre++)
        {
            if(parseInt(laboratorio.checkName[cre].substring(5).split("-")[1])==idOrari)
            {
                document.querySelector("input[name=\""+laboratorio.checkName[cre]+"\"]").disabled = status;
                document.querySelector("input[name=\""+laboratorio.checkName[cre]+"\"]").readonly = status;
            }
        }
    },
    changeLab : function() {//this the select lab
        document.querySelector(".information").innerHTML = "Attendi";
        document.querySelector(".information").className = "information positive";
        laboratorio.hourName = new Array();
        laboratorio.checkName = new Array();
        laboratorio.checkClassi = new Array();
        laboratorio.checkCategoria = new Array();
        if(this.name=="LABaddlaboratorio")//If is a Add there aren't any problem and we only print the grid
        {
            ModStatus = "AddLab";
            laboratorio.generateLabGrid(false);
            document.querySelector("button[name=\""+this.name+"\"]").className = "laboratorioItem laboratorioItem-select";
            document.querySelector(".information").innerHTML = "";
            document.querySelector(".information").className = "information";
        }
        else//In the case it's an edit we have to recovery from the server all parameter
        {
            ModStatus = "ModLab";
            var xhr = new XMLHttpRequest();//richiesta dati
            xhr.open("GET", "/api/v1/coursedetail/"+this.name.substring(3)+"?apikey="+apikey, true);
            xhr.onload = function (e) {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        var result = JSON.parse(xhr.responseText, function (key, value) {
                            var type;
                            if (value && typeof value === 'object') {
                                type = value.type;
                                if (typeof type === 'string' && typeof window[type] === 'function') {
                                    return new (window[type])(value);
                                }
                            }
                            return value;
                        });
                        if(result.message == "OK")
                        {
                            laboratorio.generateLabGrid(true);
                            document.querySelector("input[name=\"nomeLab\"]").value = result.laboratorio.nomeLaboratorio;
                            document.querySelector("input[name=\"descriptionLab\"]").value = result.laboratorio.descrizione;
                            document.querySelector("input[name=\"aulaLab\"]").value = result.laboratorio.aula;
                            document.querySelector("input[name=\"limitLab\"]").value = result.laboratorio.limite;
                            document.querySelector("input[name=\"durationLab\"]").value = result.laboratorio.durata;
                            for(var c = 0; c < result.laboratorio.orariQuestoLab.length; c++)
                            {
                                document.querySelector("input[name=\"ORA"+result.laboratorio.orariQuestoLab[c].idOrario+"\"]").disabled = false;
                                document.querySelector("input[name=\"ORA"+result.laboratorio.orariQuestoLab[c].idOrario+"\"]").readonly = false;
                                document.querySelector("input[name=\"ORA"+result.laboratorio.orariQuestoLab[c].idOrario+"\"]").checked = true;
                                for(var cre=0;cre<laboratorio.checkName.length;cre++)
                                {
                                    var splitCheck = laboratorio.checkName[cre].substring(5).split("-");
                                    if(parseInt(splitCheck[1])==result.laboratorio.orariQuestoLab[c].idOrario)
                                    {
                                        document.querySelector("input[name=\""+laboratorio.checkName[cre]+"\"]").disabled = false;
                                        document.querySelector("input[name=\"ORA"+result.laboratorio.orariQuestoLab[c].idOrario+"\"]").readonly = false;
                                        if(result.laboratorio.orariQuestoLab[c].classi.indexOf(splitCheck[0]) != -1)
                                        {
                                            document.querySelector("input[name=\""+laboratorio.checkName[cre]+"\"]").checked = true;
                                        }
                                    }
                                }
                            }
                            laboratorio.checkHourPoint();
                            document.querySelector(".information").innerHTML = "";
                            document.querySelector(".information").className = "information";
                            request = false;
                        }
                        else if(result.message == "No hours in the system")
                        {
                            document.querySelector(".information").innerHTML = "Non ci sono ore nel sistema";
                            document.querySelector(".information").className = "information error";
                            if(debugSettings.forceLogout) { logout(); }
                            request = false;
                        }
                        else if(result.message == "Api Key is not valid")
                        {
                            document.querySelector(".information").innerHTML = "Login errato";
                            document.querySelector(".information").className = "information error";
                            if(debugSettings.forceLogout) { logout(); }
                            request = false;
                        }
                        else
                        {
                            document.querySelector(".information").innerHTML = result.message;
                            document.querySelector(".information").className = "information error";
                            if(debugSettings.forceLogout) { logout(); }
                            request = false;
                        }
                        if(debugSettings.requestOutput) { console.log(xhr.responseText); }
                    } else {
                        document.querySelector(".information").innerHTML = "Errore del server, riprova più tardi";
                        document.querySelector(".information").className = "information error";
                        if(debugSettings.forceLogout) { logout(); }
                        console.error(xhr.statusText);
                        request = false;
                    }
                }
            };
            xhr.onerror = function (e) {
                document.querySelector(".information").innerHTML = "Errore nella richiesta, riprova!!!";
                document.querySelector(".information").className = "information error";
                if(debugSettings.forceLogout) { logout(); }
            };
            xhr.send();
        }
    },
    generateLabGrid : function(withHourRemain) {
        var println = "<form id=\"savageForm\"><div class=\"data-element\"><div class=\"row data\"><div class=\"col-md-4 table-intestation\">Nome Laboratorio:</div><div class=\"col-md-8 fields-name\"><input type=\"text\" class=\"field\" name=\"nomeLab\"></div></div><div class=\"row data\"><div class=\"col-md-4 table-intestation\">Descrizione Laboratorio:</div><div class=\"col-md-8 fields-description\"><input type=\"text\" class=\"field\" name=\"descriptionLab\"></div></div><div class=\"row data\"><div class=\"col-md-4 table-intestation\">Aula:</div><div class=\"col-md-8 fields-class\"><input type=\"text\" class=\"field\" name=\"aulaLab\"></div></div><div class=\"row data\"><div class=\"col-md-4 table-intestation\">Limite:</div><div class=\"col-md-8 fields-limit\"><input type=\"number\" class=\"field\" min=\"10\" value=\"20\" max=\"100\" name=\"limitLab\"></div></div><div class=\"row data\"><div class=\"col-md-4 table-intestation\">Durata:</div><div class=\"col-md-8 fields-duration\"><input type=\"number\" class=\"field\" min=\"1\" value=\"1\" max=\"5\" name=\"durationLab\"></div></div></div><div class=\"table-responsive hour-list\"><table class=\"table\"><thead><tr><th>Classe:</th>";
        for(var d=0;d<laboratorio.elencoOrari.length;d++)
        {
            var dataOra = new Date(laboratorio.elencoOrari[d].orarioinizio.replace(" ","T"));
            println += "<th><input type=\"checkbox\" name=\"ORA"+laboratorio.elencoOrari[d].idorari+"\"><br/>"+dataOra.toLocaleDateString()+" "+dataOra.getHours()+":"+dataOra.getMinutes()+"</th>";
            laboratorio.hourName.push("ORA"+laboratorio.elencoOrari[d].idorari);
        }
        println += "</tr></thead><tbody>";
        var classiSort = laboratorio.elencoClassi;
        for(var d=0;d<classiSort.length;d++)
        {
            classiSort[d]["showed"] = false;
        }
        var cond = true;
        while(cond)
        {
            var p = 0;
            var found = true;
            while(found)
            {
                if(classiSort[p].showed == false)
                {
                    println += "<tr><td class=\"class-category\" colspan=\""+(1+laboratorio.elencoOrari.length)+"\"><input class=\"cumulative-category\" type=\"checkbox\" name=\"CAT"+classiSort[p].categoria+"\">"+classiSort[p].categoria+"</td></tr>";
                    laboratorio.checkCategoria.push("CAT"+classiSort[p].categoria);
                    for(var c = p; c < classiSort.length; c++)
                    {
                        if(classiSort[c].categoria==classiSort[p].categoria)
                        {
                            println += "<tr><td class=\"class-element\"><input class=\"cumulative-class\" type=\"checkbox\" name=\"CLA"+classiSort[c].classe+"-"+classiSort[c].categoria+"\">"+classiSort[c].classe+"</td>";
                            laboratorio.checkClassi.push("CLA"+classiSort[c].classe+"-"+classiSort[c].categoria);
                            for(var orarioInt = 0; orarioInt < laboratorio.elencoOrari.length; orarioInt++)
                            {
                                println += "<td><input type=\"checkbox\" name=\"CHECK"+classiSort[c].classe+"-"+laboratorio.elencoOrari[orarioInt].idorari+"\"></td>";
                                laboratorio.checkName.push("CHECK"+classiSort[c].classe+"-"+laboratorio.elencoOrari[orarioInt].idorari);
                            }
                            println += "</tr>";
                            classiSort[c].showed = true;
                        }
                    }
                    found = false;
                }
                else
                {
                    p++;
                }
            }
            var rimanenti = 0;
            for(var d=0;d<classiSort.length;d++) {
                if(classiSort[d].showed == false) {
                    rimanenti++;
                }
            }
            if(rimanenti<=0) {
                cond = false;
            }
        }
        println += "</tbody></table></div><button type=\"submit\" class=\"float btn\">SALVA</button></form>";
        document.querySelector(".laboratorio-detail").innerHTML = println;
        document.getElementById("savageForm").addEventListener("submit", laboratorio.saveData);
        document.querySelector("input[name=\"durationLab\"]").onchange = laboratorio.durationChange;
        for(var p=0;p<laboratorio.hourName.length;p++) {
            document.querySelector("input[name=\""+laboratorio.hourName[p]+"\"]").onchange = laboratorio.hourChange;
        }
        for(var p=0; p<laboratorio.checkName.length;p++)
        {
            document.querySelector("input[name=\""+laboratorio.checkName[p]+"\"]").onchange = laboratorio.checkChange;
            document.querySelector("input[name=\""+laboratorio.checkName[p]+"\"]").disabled = true;
            document.querySelector("input[name=\""+laboratorio.checkName[p]+"\"]").readonly = true;
        }
        for(var p=0;p<laboratorio.checkClassi.length;p++) {
            document.querySelector("input[name=\""+laboratorio.checkClassi[p]+"\"]").onchange = laboratorio.checkToClasse;
        }
        for(var p=0;p<laboratorio.checkCategoria.length;p++) {
            document.querySelector("input[name=\""+laboratorio.checkCategoria[p]+"\"]").onchange = laboratorio.checkToCategoria;
        }
        for(var c=0; c<laboratorio.elencoLaboratori.length;c++) {
            document.querySelector("button[name=\""+laboratorio.elencoLaboratori[c]+"\"]").className = "laboratorioItem";
        }
    },
    checkToCategoria : function() {
        var categoria = this.name.substring(3);
        for(var p=0;p<laboratorio.checkClassi.length;p++) {
            if(laboratorio.checkClassi[p].substring(3).split("-")[1]==categoria) {
                document.querySelector("input[name=\""+laboratorio.checkClassi[p]+"\"]").checked = this.checked;
                laboratorio.checkToClasseByName(laboratorio.checkClassi[p].substring(3).split("-")[0],this.checked);
            }
        }
    },
    durationChange : function() {
        laboratorio.checkHourPoint();
    },
    checkToClasse : function() {
        laboratorio.checkToClasseByName(this.name.substring(3).split("-")[0],this.checked);
    },
    hourChange : function() {
        var checkHour = parseInt(this.name.substring(3));
        for(var p=0;p<laboratorio.checkName.length;p++)
        {
            if(parseInt(laboratorio.checkName[p].substring(5).split("-")[1])==checkHour)
            {
                if(laboratorio.checked)
                {
                    document.querySelector("input[name=\""+laboratorio.checkName[p]+"\"]").disabled = false;
                    document.querySelector("input[name=\""+laboratorio.checkName[p]+"\"]").readonly = false;
                }
                else
                {
                    document.querySelector("input[name=\""+laboratorio.checkName[p]+"\"]").disabled = true;
                    document.querySelector("input[name=\""+laboratorio.checkName[p]+"\"]").readonly = true;
                }
            }
        }
        laboratorio.checkHourPoint();
    },
    saveData : function(e) {
        e.preventDefault();
        if(ModStatus=="AddLab")
        {
            document.querySelector(".information").innerHTML = "Attendi";
            document.querySelector(".information").className = "information positive";
            var xhr = new XMLHttpRequest();//richiesta dati
            xhr.open("POST", "/api/v1/laboratory?apikey="+apikey, true);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            var params = "";
            params += "name=" + document.querySelector("input[name=\"nomeLab\"]").value;
            params += "&description=" + document.querySelector("input[name=\"descriptionLab\"]").value;
            params += "&classroom=" + document.querySelector("input[name=\"aulaLab\"]").value;
            params += "&limit=" + document.querySelector("input[name=\"limitLab\"]").value;
            params += "&duration=" + document.querySelector("input[name=\"durationLab\"]").value;
            for(var p=0;p<laboratorio.hourName.length;p++) {
                if(document.querySelector("input[name=\""+laboratorio.hourName[p]+"\"]").checked==true&&document.querySelector("input[name=\""+laboratorio.hourName[p]+"\"]").disabled==false) {
                    params += "&hours[]=" + parseInt(laboratorio.hourName[p].substring(3));
                }
            }
            var classArray = "";
            var hourArray = "";
            for(var p=0;p<laboratorio.checkName.length;p++) {
                if(document.querySelector("input[name=\""+laboratorio.checkName[p]+"\"]").checked==true&&document.querySelector("input[name=\""+laboratorio.checkName[p]+"\"]").disabled==false) {
                    classArray += "&class[]=" + laboratorio.checkName[p].substring(5).split("-")[0];
                    hourArray += "&classHours[]=" + parseInt(laboratorio.checkName[p].substring(5).split("-")[1]);
                }
            }
            //trasmissione di valori assoluti
            params += classArray + hourArray;
            xhr.onload = function (e) {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        var result = JSON.parse(xhr.responseText, function (key, value) {
                            var type;
                            if (value && typeof value === 'object') {
                                type = value.type;
                                if (typeof type === 'string' && typeof window[type] === 'function') {
                                    return new (window[type])(value);
                                }
                            }
                            return value;
                        });
                        if(result.message == "OK")
                        {
                            document.querySelector(".information").innerHTML = "";
                            document.querySelector(".information").className = "information";
                            request = false;
                        }
                        else if(result.message == "No hours in the system")
                        {
                            document.querySelector(".information").innerHTML = "Non ci sono ore nel sistema";
                            document.querySelector(".information").className = "information error";
                            if(debugSettings.forceLogout) { logout(); }
                            request = false;
                        }
                        else if(result.message == "Api Key is not valid")
                        {
                            document.querySelector(".information").innerHTML = "Login errato";
                            document.querySelector(".information").className = "information error";
                            if(debugSettings.forceLogout) { logout(); }
                            request = false;
                        }
                        else
                        {
                            document.querySelector(".information").innerHTML = result.message;
                            document.querySelector(".information").className = "information error";
                            if(debugSettings.forceLogout) { logout(); }
                            request = false;
                        }
                        if(debugSettings.requestOutput) { console.log(xhr.responseText); }
                    } else {
                        document.querySelector(".information").innerHTML = "Errore del server, riprova più tardi";
                        document.querySelector(".information").className = "information error";
                        if(debugSettings.forceLogout) { logout(); }
                        console.error(xhr.statusText);
                        request = false;
                    }
                }
            };
            xhr.onerror = function (e) {
                document.querySelector(".information").innerHTML = "Errore nella richiesta, riprova!!!";
                document.querySelector(".information").className = "information error";
                if(debugSettings.forceLogout) { logout(); }
            };
            xhr.send(params);
        }
        else if(ModStatus=="ModLab")
        {
            modalBox.show("Conferma","",function(){});
            //TODO: add update date
        }
    },
    modConfermation : function() {

    },
    checkChange : function() {
        
    },
    checkToClasseByName : function(name,status) {
        for(var p=0;p<laboratorio.checkName.length;p++) {
            if(laboratorio.checkName[p].substring(5).split("-")[0]==name&&document.querySelector("input[name=\""+laboratorio.checkName[p]+"\"]").disabled==false) {
                document.querySelector("input[name=\""+laboratorio.checkName[p]+"\"]").checked = status;
            }
        }
    }
};
var classi = {
    classiList : [],
    isEdit : false,
    editClass : null,
    lastCategory : 0,
    show : function() {
        changeNavPosition("classi-nav");
        document.querySelector(".information").innerHTML = "Attendi";
        document.querySelector(".information").className = "information positive";
        var xhr = new XMLHttpRequest();//richiesta dati
        xhr.open("GET", "/api/v1/class?apikey="+apikey, true);
        xhr.onload = function (e) {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    var result = JSON.parse(xhr.responseText, function (key, value) {
                        var type;
                        if (value && typeof value === 'object') {
                            type = value.type;
                            if (typeof type === 'string' && typeof window[type] === 'function') {
                                return new (window[type])(value);
                            }
                        }
                        return value;
                    });
                    if(result.message == "OK") {
                        classi.classiList = result.classi;
                        classi.reloadTable();
                        document.querySelector(".information").innerHTML = "";
                        document.querySelector(".information").className = "information";
                        request = false;
                    }
                    else if(result.message == "Classi not insert in the system") {
                        classi.classiList = [];
                        classi.reloadTable();
                        document.querySelector(".information").innerHTML = "";
                        document.querySelector(".information").className = "information";
                        request = false;
                    }
                    else if(result.message == "Api Key is not valid")
                    {
                        document.querySelector(".information").innerHTML = "Login errato";
                        document.querySelector(".information").className = "information error";
                        if(debugSettings.forceLogout) { logout(); }
                        request = false;
                    }
                    else
                    {
                        document.querySelector(".information").innerHTML = result.message;
                        document.querySelector(".information").className = "information error";
                        if(debugSettings.forceLogout) { logout(); }
                        request = false;
                    }
                    if(debugSettings.requestOutput) { console.log(xhr.responseText); }
                } else {
                    document.querySelector(".information").innerHTML = "Errore del server, riprova più tardi";
                    document.querySelector(".information").className = "information error";
                    if(debugSettings.forceLogout) { logout(); }
                    console.error(xhr.statusText);
                    request = false;
                }
            }
        };
        xhr.onerror = function (e) {
            document.querySelector(".information").innerHTML = "Errore nella richiesta, riprova!!!";
            document.querySelector(".information").className = "information error";
            if(debugSettings.forceLogout) { logout(); }
        };
        xhr.send();
    },
    addClick : function() {
        classi.isEdit = false;
        classi.editClass = null;
        classi.showDetail();
    },
    editClick : function() {
        classi.isEdit = true;
        //TODO: add id categoria
        classi.showDetail();
    },
    showDetail : function() {
        document.querySelector(".information").innerHTML = "Attendi";
        document.querySelector(".information").className = "information positive";
        var println = "<div class=\"container-toolbar\"><ul class=\"toolbar\"><li class=\"back-button\"><i class=\"fa fa-arrow-left\" aria-hidden=\"true\"></i>Indietro</li></ul></div><p class=\"intestation\">";
        if(classi.isEdit) {
            println += "Modifica Classe:";
        }
        else {
            println += "Aggiungi classe:";
        }
        println += "</p><form id=\"editForm\"><div class=\"data-element\"><div class=\"row data\"><div class=\"col-md-4 table-intestation\">Classe:</div><div class=\"col-md-8 fields-name\"><input type=\"text\" class=\"field\" name=\"classe\"></div></div><div class=\"data-element\"><div class=\"row data\"><div class=\"col-md-4 table-intestation\">Categoria:</div><div class=\"col-md-8 fields-name\"><select name=\"category\" class=\"field\"><option value=\"((selectCategory))\">Seleziona una categoria...</option>";
        var resultList = [];
        for(var c=0; c < classi.classiList.length; c++) {
            if(resultList.indexOf(classi.classiList[c].categoria)==-1) {
                resultList = resultList.concat([classi.classiList[c].categoria]);
            }
        }
        for(var c = 0; c < resultList.length; c++) {
            println += "<option value=\""+resultList[c]+"\">"+resultList[c]+"</option>";
        }
        println += "<option value=\"((addNewCategory))\">Aggiungi nuova categoria</option>";
        println += "</select></div></div><button type=\"submit\" class=\"float btn\">SALVA</button></div></form>";
        document.querySelector(".show-navigation-bar").innerHTML = println;
        document.querySelector("select[name=\"category\"]").addEventListener("change",classi.addCategoryModal);
        document.querySelector(".back-button").onclick = classi.backClick;
        document.getElementById("editForm").addEventListener("submit", classi.saveData);
        document.querySelector(".information").innerHTML = "";
        document.querySelector(".information").className = "information";
    },
    addCategoryModal : function(e) {
        if(e.target.options[e.target.selectedIndex].value=="((addNewCategory))") {
            modalBox.show("Aggiungi una categoria","<form class=\"addCategory\"><div class=\"form-group\"><input type=\"text\" class=\"form-control no-round-border\" name=\"categoryInsert\" placeholder=\"Categoria\"></div><button type=\"submit\" class=\"btn btn-primary no-margin-top\">Aggiungi</button></form>",classi.closeCategory);
            document.querySelector(".addCategory").addEventListener("submit",classi.addCategory);
        }
        else {
            classi.lastCategory = e.target.selectedIndex;
        }
    },
    closeCategory : function() {
        document.querySelector("select[name=\"category\"]").selectedIndex = classi.lastCategory;
    },
    addCategory : function(e) {
        e.preventDefault();
        var categoryList = document.querySelector("select[name=\"category\"]").options;
        var category = document.querySelector("input[name=\"categoryInsert\"]").value;
        categoryList.add(new Option(category, category), categoryList[document.querySelector("select[name=\"category\"]").options.length-1]);
        var selectIndex = 0;
        for(var c=0; c<categoryList.length; c++) {
            if(categoryList[c].value == category) {
                selectIndex = c;
            };
        }
        var evt = document.createEvent("HTMLEvents");
        categoryList.selectedIndex = selectIndex;
        evt.initEvent("change", true, true);
        document.querySelector("select[name=\"category\"]").dispatchEvent(evt);
        modalBox.close();
    },
    backClick : function() {
        classi.show();
    },
    saveData : function(e) {
        e.preventDefault();
        document.querySelector(".information").innerHTML = "Attendi";
        document.querySelector(".information").className = "information positive";
        var category = document.querySelector("select[name=\"category\"]").options[document.querySelector("select[name=\"category\"]").selectedIndex].value;
        var classe = document.querySelector("input[name=\"classe\"]").value;
        if(category == "((addNewCategory))" || category == "((selectCategory))") {
            document.querySelector(".information").innerHTML = "Seleziona una categoria";
            document.querySelector(".information").className = "information error";
        }
        else if(classe.length == 0) {
            document.querySelector(".information").innerHTML = "La classe non può essere vuota";
            document.querySelector(".information").className = "information error";
        }
        else {
            var xhr = new XMLHttpRequest();//richiesta dati
            if(classi.isEdit) {
                xhr.open("PUT", "/api/v1/class?apikey="+apikey, true);
                xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                var params = "";
                params += "oldClasse=" + classi.editClass.classe;
                params += "&newClasse=" + classe;
                params += "&categoria=" + category;
            }
            else {
                xhr.open("POST", "/api/v1/class?apikey="+apikey, true);
                xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                var params = "";
                params += "classe=" + classe;
                params += "&categoria=" + category;
            }
            xhr.onload = function (e) {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        var result = JSON.parse(xhr.responseText, function (key, value) {
                            var type;
                            if (value && typeof value === 'object') {
                                type = value.type;
                                if (typeof type === 'string' && typeof window[type] === 'function') {
                                    return new (window[type])(value);
                                }
                            }
                            return value;
                        });
                        if(result.message == "OK")
                        {
                            request = false;
                            if(classi.isEdit) {
                                for(var c=0; c<classi.classiList.length;c++) {
                                    if(classi.classiList[c].classe == classi.editClass.classe) {
                                        classi.classiList[c] = {
                                            "classe" : classe,
                                            "categoria" : category
                                        };
                                    }
                                }
                            }
                            else {
                                classi.classiList[classi.classiList.length] = {
                                    "classe" : classe,
                                    "categoria" : category
                                };
                            }
                            classi.reloadTable();
                            document.querySelector(".information").innerHTML = "";
                            document.querySelector(".information").className = "information";
                        }
                        else if(result.message == "Api Key is not valid")
                        {
                            document.querySelector(".information").innerHTML = "Login errato";
                            document.querySelector(".information").className = "information error";
                            if(debugSettings.forceLogout) { logout(); }
                            request = false;
                        }
                        else
                        {
                            document.querySelector(".information").innerHTML = result.message;
                            document.querySelector(".information").className = "information error";
                            if(debugSettings.forceLogout) { logout(); }
                            request = false;
                        }
                        if(debugSettings.requestOutput) { console.log(xhr.responseText); }
                    } else {
                        document.querySelector(".information").innerHTML = "Errore del server, riprova più tardi";
                        document.querySelector(".information").className = "information error";
                        if(debugSettings.forceLogout) { logout(); }
                        console.error(xhr.statusText);
                        request = false;
                    }
                }
            };
            xhr.onerror = function (e) {
                document.querySelector(".information").innerHTML = "Errore nella richiesta, riprova!!!";
                document.querySelector(".information").className = "information error";
                if(debugSettings.forceLogout) { logout(); }
            };
            xhr.send(params);
        }
    },
    reloadTable : function() {
        document.querySelector(".show-navigation-bar").innerHTML = "<ul class=\"toolbar\"><li class=\"add-button-toolbar\"><i class=\"fa fa-plus\" aria-hidden=\"true\"></i>Aggiungi</li><li><i class=\"fa fa-upload\" aria-hidden=\"true\"></i>Importa</li><li><i class=\"fa fa-download\" aria-hidden=\"true\"></i>Esporta</li><li><i class=\"fa fa-cloud-download\" aria-hidden=\"true\"></i>Scarica modello importazione</li></ul><div class=\"table-responsive classi-table\"></div>";
        document.querySelector(".add-button-toolbar").onclick = classi.addClick;
        if(classi.classiList.length == 0) {
            document.querySelector(".classi-table").innerHTML = "<table class=\"table\"><thead><th>Classe:</th><th>Categoria:</th><th>Cancella</th><th>Modifica</th></thead><tr><td colspan=\"4\" class=\"table-message-central\">Nessuna classe inserita</td></tr></table>";
        }
        else {
            var printTable = "<table class=\"table\"><thead><th>Classe:</th><th>Categoria:</th><th>Cancella</th><th>Modifica</th></thead>";
            for(var c=0;c<classi.classiList.length;c++)
            {
                printTable += "<tr><td>" + classi.classiList[c].classe + "</td><td>" + classi.classiList[c].categoria + "</td><td><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></td><td><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></td></tr>"
            }
            printTable += "</table>";
            document.querySelector(".classi-table").innerHTML = printTable;
        }
    }
};
var modalBox = {
    title : "",
    content : "",
    callbackFunctionClose : null,
    show : function(title, content, callbackFunctionClose) {
        modalBox.title = title;
        modalBox.content = content;
        modalBox.callbackFunctionClose = callbackFunctionClose;
        var modalDiv = document.createElement("div");
        modalDiv.className = "modalDialog";
        modalDiv.innerHTML = "<div><div class=\"modalTop\"><div class=\"modalTitle\">"+title+"</div><div title=\"Chiudi\" class=\"modalClose\">X</div></div><div class=\"modalContent\">"+content+"</div></div>";
        window.document.body.appendChild(modalDiv);
        document.querySelector(".modalDialog > div").addEventListener("click",modalBox.internalClickPropagationStop);
        document.querySelector(".modalClose").addEventListener("click",modalBox.close);
        document.querySelector(".modalDialog").addEventListener("click",modalBox.closeExternalClick);
    },
    closeExternalClick :function(e) {
        modalBox.close();
        e.stopPropagation();
    },
    internalClickPropagationStop : function(e) {
        e.stopPropagation();
    },
    close : function() {
        modalBox.callbackFunctionClose();
        modalBox.closeUI();
    },
    closeUI : function() {
        window.document.body.removeChild(document.querySelector(".modalDialog"));
    }
};
var changeNavPosition = function(i) {
    var elementList = document.querySelectorAll(".navigation-bar > li");
    for(var p=0;p<elementList.length;p++) {
        elementList.item(p).classList.remove("select-nav");
    }
    var changer = document.querySelector("." + i);
    if(changer != null) {
        changer.classList.add("select-nav");
    }
};
window.onload = function() {
    apikey = location.href.split("?")[1].split("=")[1];
    laboratorio.show();
    document.querySelector(".btn-logout").onclick = logout;
    document.querySelector(".laboratori-nav").onclick = laboratorio.show;
    document.querySelector(".classi-nav").onclick = classi.show;
}
