var apikey = "";
var request = true;
var idLaboratorio = 0;
var nameSelect = new Array();
var logout = function() {
    document.querySelector(".information").innerHTML = "Attendi";
    document.querySelector(".information").className = "information positive";
    var xhr = new XMLHttpRequest();//richiesta dati
    xhr.open("DELETE", "/api/v1/login?apikey="+apikey, true);
    xhr.onload = function (e) {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                var result = JSON.parse(xhr.responseText, function (key, value) {
                    var type;
                    if (value && typeof value === 'object') {
                        type = value.type;
                        if (typeof type === 'string' && typeof window[type] === 'function') {
                            return new (window[type])(value);
                        }
                    }
                    return value;
                });
                if(result.message == "OK")
                {
                    location.href = "/index.html";
                }
                else
                {
                    document.querySelector(".information").innerHTML = result.message;
                    document.querySelector(".information").className = "information error";
                    location.href = "/index.html";
                    request = false;
                }
                console.log(xhr.responseText);
            } else {
                document.querySelector(".information").innerHTML = "Errore del server, riprova più tardi";
                document.querySelector(".information").className = "information error";
                location.href = "/index.html";
                console.error(xhr.statusText);
                request = false;
            }
        }
    };
    xhr.onerror = function (e) {
        document.querySelector(".information").innerHTML = "Errore nella richiesta, riprova!!!";
        document.querySelector(".information").className = "information error";
        location.href = "/index.html";
    };
    xhr.send();
}
var checkChange = function() {
    document.querySelector(".information").innerHTML = "Attendi";
    document.querySelector(".information").className = "information positive";
    var callName = this.name;
    var xhr = new XMLHttpRequest();//richiesta dati
    xhr.open("GET", "/api/v1/bookedstudent/"+idLaboratorio+"/"+this.name+"?apikey="+apikey, true);
    xhr.onload = function (e) {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                var result = JSON.parse(xhr.responseText, function (key, value) {
                    var type;
                    if (value && typeof value === 'object') {
                        type = value.type;
                        if (typeof type === 'string' && typeof window[type] === 'function') {
                            return new (window[type])(value);
                        }
                    }
                    return value;
                });
                if(result.message == "OK")
                {
                    for(var d=0;d<nameSelect.length;d++)
                    {
                        document.querySelector("button[name=\""+nameSelect[d]+"\"]").className = "turn";
                    }
                    document.querySelector("button[name=\""+callName+"\"]").className = "turn turn-select";
                    if(result.studenti!=null)
                    {
                        var println = "<p class=\"intestation\">Lista Studenti:</p><table class=\"table\"><thead><tr><th>N:</th><th>Nome:</th><th>Cognome:</th><th>Classe:</th></tr></thead><tbody>";
                        for(var c = 0; c < result.studenti.length; c++)
                        {
                            println += "<tr><td>"+(c+1)+"</td><td>"+result.studenti[c].nome+"</td><td>"+result.studenti[c].cognome+"</td><td>"+result.studenti[c].classe+"</td></tr>";
                        }
                        println += "</tbody></table>";
                        document.querySelector(".studentList").innerHTML = println;
                    }
                    else
                    {
                        document.querySelector(".studentList").innerHTML = "<p class=\"intestation\">Lista Studenti:</p>Nessuno studente iscritto";
                    }
                    document.querySelector(".information").innerHTML = "";
                    document.querySelector(".information").className = "information";
                    request = false;
                }
                else if(result.message == "Forbidden operation for this user level")
                {
                    document.querySelector(".information").innerHTML = "Operazione non permessa";
                    document.querySelector(".information").className = "information error";
                    logout();
                    request = false;
                }
                else if(result.message == "Api Key is not valid")
                {
                    document.querySelector(".information").innerHTML = "Login errato";
                    document.querySelector(".information").className = "information error";
                    logout();
                    request = false;
                }
                else
                {
                    document.querySelector(".information").innerHTML = result.message;
                    document.querySelector(".information").className = "information error";
                    logout();
                    request = false;
                }
                console.log(xhr.responseText);
            } else {
                document.querySelector(".information").innerHTML = "Errore del server, riprova più tardi";
                document.querySelector(".information").className = "information error";
                logout();
                console.error(xhr.statusText);
                request = false;
            }
        }
    };
    xhr.onerror = function (e) {
        document.querySelector(".information").innerHTML = "Errore nella richiesta, riprova!!!";
        document.querySelector(".information").className = "information error";
        logout();
    };
    xhr.send();
}
window.onload = function() {
    document.querySelector(".information").innerHTML = "Attendi";
    document.querySelector(".information").className = "information positive";
    apikey = location.href.split("?")[1].split("=")[1];
    var xhr = new XMLHttpRequest();//richiesta dati
    xhr.open("GET", "/api/v1/assignedlab?apikey="+apikey, true);
    xhr.onload = function (e) {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                var result = JSON.parse(xhr.responseText, function (key, value) {
                    var type;
                    if (value && typeof value === 'object') {
                        type = value.type;
                        if (typeof type === 'string' && typeof window[type] === 'function') {
                            return new (window[type])(value);
                        }
                    }
                    return value;
                });
                if(result.message == "OK")
                {
                    idLaboratorio = result.laboratorio.idLaboratorio;
                    document.querySelector(".fields-name").innerHTML = result.laboratorio.nomeLaboratorio;
                    document.querySelector(".fields-description").innerHTML = result.laboratorio.descrizione;
                    document.querySelector(".fields-class").innerHTML = result.laboratorio.aula;
                    document.querySelector(".fields-limit").innerHTML = result.laboratorio.limite;
                    document.querySelector(".fields-duration").innerHTML = result.laboratorio.durata;
                    var println = "<p class=\"intestation\">Turni:</p>";
                    nameSelect = new Array();
                    for(var c = 0; c < result.laboratorio.disponibilita.length; c++)
                    {
                        nameSelect.push(result.laboratorio.disponibilita[c].idOrario+"");
                        var dataOra = new Date(result.laboratorio.disponibilita[c].dataInizio.replace(" ","T"));
                        println += "<button class=\"turn\" name=\""+result.laboratorio.disponibilita[c].idOrario+"\">"+dataOra.toLocaleDateString()+" "+dataOra.getHours()+":"+dataOra.getMinutes()+"</button>";
                    }
                    document.querySelector(".turni").innerHTML = println;
                    for(var c = 0; c < nameSelect.length; c++)
                    {
                        document.querySelector("button[name=\""+nameSelect[c]+"\"]").onclick = checkChange;
                    }
                    document.querySelector(".information").innerHTML = "";
                    document.querySelector(".information").className = "information";
                    request = false;
                }
                else if(result.message == "Laboratori not insert for this user")
                {
                    document.querySelector(".information").innerHTML = "Laboratori non inseriti";
                    document.querySelector(".information").className = "information error";
                    logout();
                    request = false;
                }
                else if(result.message == "No hours in the system")
                {
                    document.querySelector(".information").innerHTML = "Non ci sono ore nel sistema";
                    document.querySelector(".information").className = "information error";
                    logout();
                    request = false;
                }
                else if(result.message == "Api Key is not valid")
                {
                    document.querySelector(".information").innerHTML = "Login errato";
                    document.querySelector(".information").className = "information error";
                    logout();
                    request = false;
                }
                else
                {
                    document.querySelector(".information").innerHTML = result.message;
                    document.querySelector(".information").className = "information error";
                    logout();
                    request = false;
                }
                console.log(xhr.responseText);
            } else {
                document.querySelector(".information").innerHTML = "Errore del server, riprova più tardi";
                document.querySelector(".information").className = "information error";
                logout();
                console.error(xhr.statusText);
                request = false;
            }
        }
    };
    xhr.onerror = function (e) {
        document.querySelector(".information").innerHTML = "Errore nella richiesta, riprova!!!";
        document.querySelector(".information").className = "information error";
        logout();
    };
    xhr.send();
    document.querySelector(".btn-logout").onclick = logout;
}