window.onload = function() {
    var remainSecond = "";
    var generateNumber = Math.round(Math.random() * 20);
    var generate = [4,20,10,12,12,16,40,8,18,16,5,8,12,14,8,8,12,12,8,4];
    var path = "/img/gif/"+generateNumber+".gif";
    document.querySelector(".gif-dynamic").src = path;
    remainSecond = generate[generateNumber];
    setInterval(function() {
        if(remainSecond<=0)
        {
            window.location.href = "/index.html";
        }
        else
        {
            remainSecond--;
            document.querySelector(".message-redirect").innerHTML = "Redirect alla pagina principale tra "+remainSecond+" secondi";
        }
    },1000);
}