<?php
class settings {
    private $settingsPath = "config-www/cogestione.ini";
    public function loadSettings($path = "") {
        if(isset($path)==false||strcmp("",$path)==0)
        {
            $path = $this->settingsPath;
        };
        if(file_exists($path))
        {
            return parse_ini_file($path);
        }
        else
        {
            $this->safefilerewrite($path, "");
            return [];
        }
    }
    public function updateIni($ini, $path = "") {
        if(isset($path)==false||strcmp("",$path)==0)
        {
            $path = $this->settingsPath;
        };
        $res = array();
        foreach($ini as $key => $val)
        {
            if(is_array($val))
            {
                $res[] = "[$key]";
                foreach($val as $skey => $sval) $res[] = "$skey = ".(is_numeric($sval) ? $sval : '"'.$sval.'"');
            }
            else $res[] = "$key = ".(is_numeric($val) ? $val : '"'.$val.'"');
        }
        $this->safefilerewrite($path, implode("\r\n", $res));
    }
    
    private function safefilerewrite($fileName, $dataToSave)
    {
        if ($fp = fopen($fileName, 'w'))
        {
            $startTime = microtime(TRUE);
            do
            {
                $canWrite = flock($fp, LOCK_EX);
                // If lock not obtained sleep for 0 - 100 milliseconds, to avoid collision and CPU load
                if(!$canWrite) usleep(round(rand(0, 100)*1000));
            } while ((!$canWrite)and((microtime(TRUE)-$startTime) < 5));
            //file was locked so now we can store information
            if ($canWrite)
            {
                fwrite($fp, $dataToSave);
                flock($fp, LOCK_UN);
            }
            fclose($fp);
        }
    }
}
?>