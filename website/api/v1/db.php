<?php
class db
{
    private $mysqli;
    public function __construct() {
        require_once('settings.php');
        $settings = new Settings();
        $ini = $settings->loadSettings();
        $this->mysqli = new mysqli($ini['db_host'], $ini['db_username'], $ini['db_password'], $ini['db_dbname'], $ini['db_port']);
        if($this->mysqli->connect_errno) {
            //handle the error here
            connectionError($this->mysqli->connect_error());
        }
    }
    function __destruct() {
        $this->mysqli->close();
    }

        // =====>>>> password%!"username
    public function obtainLoginUserInfo($username, $password)
    {
        $crypto = hash('sha512', $password."%!\"".$username);
        if ($res = $this->mysqli->prepare("SELECT utenti.username, utenti.livello, utenti.nome, utenti.cognome, utenti.classi_nomeclasse FROM utenti WHERE username = '$username' AND password = '$crypto'")) {
            /* execute query */
            $res->execute();
            /* store result */
            $res->store_result();
            if($res->num_rows==0)
            {
                return null;
            }
            else
            {
                $row = array();
                $res->bind_result($row['utenti.username'],$row['utenti.livello'],$row['utenti.nome'],$row['utenti.cognome'],$row['utenti.classi_nomeclasse']); 
                $res->fetch();
                $ritorno = array();
                $ritorno['username'] = $row['utenti.username'];
                $ritorno['livello'] = $row['utenti.livello'];
                $ritorno['display'] = $row['utenti.nome'] . " " . $row['utenti.cognome'];
                $ritorno['classe'] = $row['utenti.classi_nomeclasse'];
                return $ritorno;
            }
            /* free result */
            $stmt->free_result();
            /* close statement */
            $stmt->close();
        }
        else
        {
            //handle the error here
            header("HTTP/1.0 200 OK");
            $error = $this->mysqli->error;
            echo "{\"message\":\"".$error."\"}";
            die();
        }
    }


    public function checkApiKey($apikey)
    {
        if ($res = $this->mysqli->prepare("SELECT utenti.username, utenti.livello, utenti.nome, utenti.cognome, utenti.classi_nomeclasse FROM utenti, accesslist WHERE utenti.username = accesslist.utenti_username AND accesslist.apikey = \"".$apikey."\"")) {
            /* execute query */
            $res->execute();
            /* store result */
            $res->store_result();
            if($res->num_rows==0)
            {
                return null;
            }
            else
            {
                $row = array();
                $res->bind_result($row['utenti.username'],$row['utenti.livello'],$row['utenti.nome'],$row['utenti.cognome'],$row['utenti.classi_nomeclasse']); 
                $res = $res->fetch();
                $ritorno = array();
                $ritorno['username'] = $row['utenti.username'];
                $ritorno['livello'] = $row['utenti.livello'];
                $ritorno['display'] = $row['utenti.nome'] . " " . $row['utenti.cognome'];
                $ritorno['classe'] = $row['utenti.classi_nomeclasse'];
                return $ritorno;
            }
            /* free result */
            $stmt->free_result();
            /* close statement */
            $stmt->close();
        }
        else
        {
            //handle the error here
            header("HTTP/1.0 200 OK");
            $error = $this->mysqli->error;
            echo "{\"message\":\"".$error."\"}";
            die();
        }
    }

        //apikey generation system
    //current_timestamp%"username
    public function insertApiKey($username)
    {
        $time = new DateTime();
        $timestamp = $time->format('Y-m-d H:i:s');
        $apikey = hash('sha512', $timestamp."%\"".$username);
        $res = $this->mysqli->query("INSERT INTO accesslist (apikey, ultimaoperazione, utenti_username) VALUES (\"$apikey\",\"$timestamp\", \"$username\")");
        if ( $res === false ){
            //handle the error here
            header("HTTP/1.0 200 OK");
            $error = $this->mysqli->error;
            echo "{\"message\":\"".$error."\"}";
            die();
        }
        else {
            return $apikey;
        }
    }

    public function deleteApiKey($apikey)
    {
        if ($res = $this->mysqli->prepare("DELETE FROM accesslist WHERE accesslist.apikey = ?")) {
            $res->bind_param("s", $apikey);
            /* execute query */
            $res->execute();
            $res->close();
        }
    }
    /*error manage*/
    public function connectionError($errorDescription)
    {
        //handle the error here
        header("HTTP/1.0 200 OK");
        echo "{\"message\":\"Database error\",\"type\":\"connection error\",\"errorDescription\":\"".$errorDescription."\"}";
        die();
    }
    
    public function databaseError($errorNo,$errorDescription)
    {
        //handle the error here
        header("HTTP/1.0 200 OK");
        echo "{\"message\":\"Database error\",\"type\":\"SQL error\",\"errorNo\":\"".$errorNo."\",\"errorDescription\":\"".$errorDescription."\"}";
        die();
    }
    
    public function executePrepare($sql)
    {
        if ($res = $this->mysqli->prepare($sql)) {
            return $res;
        }
        else
        {
            //handle the error here
            connectionError($this->mysqli->error);
        }
    }
    
    public function getHours()
    {
        if ($res = $this->mysqli->prepare("SELECT orari.idorari, orari.orarioinizio, orari.orariofine FROM orari ORDER BY orari.orarioinizio ASC")) {
            /* execute query */
            $res->execute();
            /* store result */
            $res->store_result();
            if($res->num_rows==0)
            {
                return null;
            }
            else
            {
                $result = array();
                $row = array();
                $res->bind_result($row['orari.idorari'],$row['orari.orarioinizio'],$row['orari.orariofine']);
                while($rom = $res->fetch())
                {
                    $ritorno = array();
                    $ritorno['idorari'] = $row['orari.idorari'];
                    $ritorno['orarioinizio'] = $row['orari.orarioinizio'];
                    $ritorno['orariofine'] = $row['orari.orariofine'];
                    $result[] = $ritorno;
                }
                return $result;
            }
            /* free result */
            $res->free_result();
            /* close statement */
            $res->close();
        }
        else
        {
            //handle the error here
            connectionError($this->mysqli->error);
        }
    }
    public function getLaboratori($username)
    {
        if ($res = $this->mysqli->prepare("SELECT labora.idlaboratorio, labora.nomeLaboratorio, labora.descrizione, labora.limite, labora.durata, labora.idorari, (labora.limite - scelti.contatore) AS mancanti FROM (SELECT laboratorio.idlaboratorio, laboratorio.nomeLaboratorio, laboratorio.descrizione, laboratorio.limite, laboratorio.durata, orari.idorari FROM utenti, classi, abilitazioni, laboratorio, orari WHERE utenti.classi_nomeclasse = classi.nomeclasse AND abilitazioni.classi_nomeclasse = classi.nomeclasse AND abilitazioni.idlaboratorio = laboratorio.idlaboratorio AND abilitazioni.idorari = orari.idorari AND utenti.username = ?) AS labora LEFT JOIN (SELECT scelte.laboratorio_has_orari_laboratorio_idlaboratorio, scelte.laboratorio_has_orari_orari_idorari, COUNT(*) AS contatore FROM scelte GROUP BY scelte.laboratorio_has_orari_laboratorio_idlaboratorio, scelte.laboratorio_has_orari_orari_idorari) AS scelti ON scelti.laboratorio_has_orari_laboratorio_idlaboratorio = labora.idlaboratorio AND scelti.laboratorio_has_orari_orari_idorari = labora.idorari;")) {
            $res->bind_param("s", $username);
            /* execute query */
            $res->execute();
            /* store result */
            $res->store_result();
            if($res->num_rows==0)
            {
                return null;
            }
            else
            {
                $result = array();
                $row = array();
                $res->bind_result($row['labora.idlaboratorio'],$row['labora.nomeLaboratorio'],$row['labora.descrizione'],$row['labora.limite'],$row['labora.durata'],$row['labora.idorari'],$row['mancanti']);
                while($rom = $res->fetch())
                {
                    if(array_key_exists($row['labora.idlaboratorio'], $result)===false)
                    {
                        $result[$row['labora.idlaboratorio']] = new laboratori($row['labora.idlaboratorio'],$row['labora.nomeLaboratorio'], $row['labora.descrizione'], $row['labora.limite'], $row['labora.durata']);
                    }
                    $mancanti = $row['mancanti'];
                    if($mancanti==null)
                    {
                        $mancanti = $row['labora.limite'];
                    }
                    $result[$row['labora.idlaboratorio']]->addOrari($row['labora.idorari'],$mancanti);
                }
                return array_values($result);
            }
            /* free result */
            $stmt->free_result();
            /* close statement */
            $stmt->close();
        }
        else
        {
            //handle the error here
            connectionError($this->mysqli->error);
        }
    }
    
    public function getAllLaboratori()
    {
        if ($res = $this->mysqli->prepare("SELECT laboratorio.idlaboratorio, laboratorio.nomeLaboratorio, laboratorio.descrizione, laboratorio.limite, laboratorio.durata, disponiblita.orari_idorari, (laboratorio.limite-COUNT(scelte.utenti_username)) AS mancanti FROM laboratorio LEFT JOIN disponiblita ON laboratorio.idlaboratorio = disponiblita.laboratorio_idlaboratorio LEFT JOIN scelte ON scelte.laboratorio_has_orari_laboratorio_idlaboratorio = disponiblita.laboratorio_idlaboratorio AND scelte.laboratorio_has_orari_orari_idorari = disponiblita.orari_idorari GROUP BY laboratorio.idlaboratorio, disponiblita.orari_idorari")) {
            /* execute query */
            $res->execute();
            /* store result */
            $res->store_result();
            if($res->num_rows==0)
            {
                return null;
            }
            else
            {
                $result = array();
                $row = array();
                $res->bind_result($row['laboratorio.idlaboratorio'],$row['laboratorio.nomeLaboratorio'],$row['laboratorio.descrizione'],$row['laboratorio.limite'],$row['laboratorio.durata'],$row['disponiblita.orari_idorari'],$row['mancanti']);
                while($rom = $res->fetch())
                {
                    if(array_key_exists($row['laboratorio.idlaboratorio'], $result)===false)
                    {
                        $result[$row['laboratorio.idlaboratorio']] = new laboratori($row['laboratorio.idlaboratorio'],$row['laboratorio.nomeLaboratorio'], $row['laboratorio.descrizione'], $row['laboratorio.limite'], $row['laboratorio.durata']);
                    };
                    if($row['disponiblita.orari_idorari']!=null)
                    {
                        $result[$row['laboratorio.idlaboratorio']]->addOrari($row['disponiblita.orari_idorari'],$row['mancanti']);
                    }
                }
                return array_values($result);
            }
            /* free result */
            $res->free_result();
            /* close statement */
            $res->close();
        }
        else
        {
            //handle the error here
            connectionError($this->mysqli->error);
        }
    }
    
    public function LaboratorioDetailed($idLaboratorio) {
        if ($res = $this->mysqli->prepare("SELECT laboratorio.idlaboratorio, laboratorio.nomeLaboratorio, laboratorio.descrizione, laboratorio.aula, laboratorio.limite, laboratorio.durata, disponiblita.orari_idorari, (laboratorio.limite-COUNT(scelte.utenti_username)) AS mancanti FROM laboratorio LEFT JOIN disponiblita ON laboratorio.idlaboratorio = disponiblita.laboratorio_idlaboratorio LEFT JOIN scelte ON scelte.laboratorio_has_orari_laboratorio_idlaboratorio = disponiblita.laboratorio_idlaboratorio AND scelte.laboratorio_has_orari_orari_idorari = disponiblita.orari_idorari GROUP BY laboratorio.idlaboratorio, disponiblita.orari_idorari HAVING laboratorio.idlaboratorio = ?")) {
            $res->bind_param("i", $idLaboratorio);
            /* execute query */
            $res->execute();
            /* store result */
            $res->store_result();
            if($res->num_rows==0)
            {
                return null;
            }
            else
            {
                $result = array();
                $row = array();
                $res->bind_result($row['laboratorio.idlaboratorio'],$row['laboratorio.nomeLaboratorio'],$row['laboratorio.descrizione'],$row['laboratorio.aula'],$row['laboratorio.limite'],$row['laboratorio.durata'],$row['disponiblita.orari_idorari'],$row['mancanti']);
                while($rom = $res->fetch())
                {
                    if(array_key_exists($row['laboratorio.idlaboratorio'], $result)===false)
                    {
                        $result[$row['laboratorio.idlaboratorio']] = new laboratori($row['laboratorio.idlaboratorio'],$row['laboratorio.nomeLaboratorio'], $row['laboratorio.descrizione'], $row['laboratorio.limite'], $row['laboratorio.durata']);
                        $result[$row['laboratorio.idlaboratorio']]->setAula($row['laboratorio.aula']);
                        $result[$row['laboratorio.idlaboratorio']]->setExtendedOrari(true);
                    }
                    if($row['disponiblita.orari_idorari']!=null)
                    {
                        $result[$row['laboratorio.idlaboratorio']]->addOrari($row['disponiblita.orari_idorari'],$row['mancanti']);
                    }
                }
                if($small = $this->mysqli->prepare("SELECT abilitazioni.idorari, abilitazioni.classi_nomeclasse FROM abilitazioni WHERE abilitazioni.idlaboratorio = ?"))
                {
                    $small->bind_param("i", $idLaboratorio);
                    /* execute query */
                    $small->execute();
                    /* store result */
                    $small->store_result();
                    $row = array();
                    $small->bind_result($row['abilitazioni.idorari'],$row['abilitazioni.classi_nomeclasse']);
                    while($rew = $small->fetch())
                    {
                        $result[$idLaboratorio]->addClasse($row['abilitazioni.idorari'], $row['abilitazioni.classi_nomeclasse']);
                    }
                    return $result[$idLaboratorio];
                }
                else
                {
                    //handle the error here
                    header("HTTP/1.0 200 OK");
                    $error = $this->mysqli->error;
                    echo "{\"message\":\"".$error."\"}";
                    die();
                }
            }
            /* free result */
            $res->free_result();
            /* close statement */
            $res->close();
        }
        else
        {
            //handle the error here
            connectionError($this->mysqli->error);
        }
    }
    
    public function GetClassi()
    {
        if ($res = $this->mysqli->prepare("SELECT classi.nomeclasse, classi.categoria FROM classi")) {
            /* execute query */
            $res->execute();
            /* store result */
            $res->store_result();
            if($res->num_rows==0)
            {
                return null;
            }
            else
            {
                $result = array();
                $row = array();
                $res->bind_result($row['classi.nomeclasse'],$row['classi.categoria']);
                while($rom = $res->fetch())
                {
                    $result[] = new classi($row['classi.nomeclasse'],$row['classi.categoria']);
                }
                return $result;
            }
            /* free result */
            $res->free_result();
            /* close statement */
            $res->close();
        }
        else
        {
            //handle the error here
            connectionError($this->mysqli->error);
        }
    }
    
    public function InsertClassi($classe, $categoria)
    {
        if ($res = $this->mysqli->prepare("INSERT INTO classi (nomeclasse, categoria) VALUES (?,?)")) {
            $result = true;
            if(!$res->bind_param("ss",$classe,$categoria)) {
                databaseError($res->errno,$res->error);
            }
            /* execute query */
            if (!$res->execute()) {
                databaseError($res->errno,$res->error);
            }
            /* close statement */
            $res->close();
            return true;
        }
        else
        {
            //handle the error here
            connectionError($this->mysqli->error);
        }
    }
    
    public function UpdateClasse($oldClasse, $newClasse, $categoria)
    {
        if ($res = $this->mysqli->prepare("UPDATE classi SET nomeclasse = ?, categoria = ? WHERE nomeclasse = ?")) {
            if(!$res->bind_param("sss",$newClasse,$categoria,$oldClasse)) {
                databaseError($res->errno,$res->error);
            }
            /* execute query */
            if (!$res->execute()) {
                databaseError($res->errno,$res->error);
            }
            /* close statement */
            $res->close();
            return true;
        }
        else
        {
            //handle the error here
            connectionError($this->mysqli->error);
        }
    }
    
    public function UpdateBookings($username, $arrayLaboratori,$arrayFasce)
    {
        if($res = $this->mysqli->prepare("DELETE FROM scelte WHERE scelte.utenti_username = ?"))
        {
            $res->bind_param("s", $username);
            $res->execute();
            /* close statement */
            $res->close();
            if ($res = $this->mysqli->prepare("INSERT INTO scelte (utenti_username, laboratorio_has_orari_laboratorio_idlaboratorio, laboratorio_has_orari_orari_idorari) VALUES (?,?,?)")) {
                for ($i = 0; $i < count($arrayLaboratori); $i++) {
                    $res->bind_param("sii", $username,$arrayLaboratori[$i],$arrayFasce[$i]);
                    /* execute query */
                    $res->execute();
                    
                }
                /* close statement */
                $res->close();
            }
            else
            {
                //handle the error here
                header("HTTP/1.0 200 OK");
                $error = $this->mysqli->error;
                echo "{\"message\":\"".$error."\"}";
                die();
            }
        }
        else
        {
            //handle the error here
            connectionError($this->mysqli->error);
        }
    }
    public function GetScelte($username)
    {
        if($res = $this->mysqli->prepare("SELECT scelte.laboratorio_has_orari_laboratorio_idlaboratorio, scelte.laboratorio_has_orari_orari_idorari FROM scelte WHERE scelte.utenti_username = ?"))
        {
            $res->bind_param("s", $username);
            /* execute query */
            $res->execute();
            /* store result */
            $res->store_result();
            if($res->num_rows==0)
            {
                return null;
            }
            else
            {
                $result = array();
                $row = array();
                $res->bind_result($row['scelte.laboratorio_has_orari_laboratorio_idlaboratorio'],$row['scelte.laboratorio_has_orari_orari_idorari']);
                while($rom = $res->fetch())
                {
                    $result[] = new scelte($row['scelte.laboratorio_has_orari_orari_idorari'], $row['scelte.laboratorio_has_orari_laboratorio_idlaboratorio']);
                }
                return $result;
            }
            /* free result */
            $res->free_result();
            /* close statement */
            $res->close();
        }
        else
        {
            //handle the error here
            connectionError($this->mysqli->error);
        }
    }
    public function GetTimetable($username)
    {
        if($res = $this->mysqli->prepare("SELECT laboratorio.nomeLaboratorio, laboratorio.aula, laboratorio.descrizione, laboratorio.durata, orari.orarioinizio FROM laboratorio, orari, scelte WHERE scelte.laboratorio_has_orari_laboratorio_idlaboratorio = laboratorio.idlaboratorio AND scelte.laboratorio_has_orari_orari_idorari = orari.idorari AND scelte.utenti_username = ?"))
        {
            $res->bind_param("s", $username);
            /* execute query */
            $res->execute();
            /* store result */
            $res->store_result();
            if($res->num_rows==0)
            {
                return null;
            }
            else
            {
                $result = array();
                $row = array();
                $res->bind_result($row['laboratorio.nomeLaboratorio'],$row['laboratorio.aula'],$row['laboratorio.descrizione'], $row['laboratorio.durata'],$row['orari.orarioinizio']);
                while($rom = $res->fetch())
                {
                    $result[] = new orario($row['laboratorio.nomeLaboratorio'], $row['laboratorio.descrizione'], $row['laboratorio.aula'], $row['orari.orarioinizio'], $row['laboratorio.durata']);
                }
                return $result;
            }
            /* free result */
            $res->free_result();
            /* close statement */
            $res->close();
        }
        else
        {
            //handle the error here
            connectionError($this->mysqli->error);
        }
    }
    public function GetStudents($idLaboratorio, $idFascia)
    {
        if($res = $this->mysqli->prepare("SELECT utenti.nome, utenti.cognome, utenti.classi_nomeclasse FROM scelte, utenti WHERE scelte.utenti_username = utenti.username AND scelte.laboratorio_has_orari_laboratorio_idlaboratorio = ? AND scelte.laboratorio_has_orari_orari_idorari = ? ORDER BY utenti.cognome, utenti.nome"))
        {
            $res->bind_param("ii", $idLaboratorio, $idFascia);
            /* execute query */
            $res->execute();
            /* store result */
            $res->store_result();
            if($res->num_rows==0)
            {
                return null;
            }
            else
            {
                $result = array();
                $row = array();
                $res->bind_result($row['utenti.nome'],$row['utenti.cognome'],$row['utenti.classi_nomeclasse']);
                while($rom = $res->fetch())
                {
                    $result[] = new studenti($row['utenti.nome'],$row['utenti.cognome'],$row['utenti.classi_nomeclasse']);
                }
                return $result;
            }
            /* free result */
            $res->free_result();
            /* close statement */
            $res->close();
        }
        else
        {
            //handle the error here
            connectionError($this->mysqli->error);
        }
    }
    public function getResponsabili($idLaboratorio)
    {
        if($res = $this->mysqli->prepare("SELECT proprietari.utenti_username, utenti.nome, utenti.cognome FROM proprietari, utenti WHERE utenti.username = proprietari.utenti_username AND proprietari.laboratorio_id = ?;"))
        {
            $res->bind_param("i", $idLaboratorio);
            /* execute query */
            $res->execute();
            /* store result */
            $res->store_result();
            if($res->num_rows==0)
            {
                return array();
            }
            else
            {
                $result = array();
                $row = array();
                $res->bind_result($row['proprietari.utenti_username'], $row['utenti.nome'], $row['utenti.cognome']);
                while($rom = $res->fetch())
                {
                    $campo = array();
                    $campo['username'] = $row['proprietari.utenti_username'];
                    $campo['cognome'] = $row['utenti.cognome'];
                    $campo['nome'] = $row['utenti.nome'];
                    $result[] = $campo;
                }
                return $result;
            }
            /* free result */
            $res->free_result();
            /* close statement */
            $res->close();
        }
        else
        {
            //handle the error here
            connectionError($this->mysqli->error);
        }
    }
    public function addLaboratorio($id, $nomeLaboratorio, $descrizione, $aula, $limite, $durata, $elencoOrari, $elencoOrariForClassi, $elencoClassiForClassi)
    {
        if($id!=null)
        {
            if ($res = $this->mysqli->prepare("DELETE FROM laboratorio WHERE laboratorio.idlaboratorio = ?"))
            {
                $res->bind_param("i", $id);
                /* execute query */
                $res->execute();
                $res->close();
            }
            else
            {
                //handle the error here
                connectionError($this->mysqli->error);
            }
        }
        if ($res = $this->mysqli->prepare("INSERT INTO laboratorio (laboratorio.nomeLaboratorio, laboratorio.descrizione, laboratorio.aula, laboratorio.limite, laboratorio.durata) VALUES (?,?,?,?,?)")) {
            $res->bind_param("sssii",$nomeLaboratorio,$descrizione,$aula,$limite,$durata);
            /* execute query */
            $res->execute();
            $idLaboratorio = $res->insert_id;
            /* close statement */
            $res->close();
            if ($res = $this->mysqli->prepare("INSERT INTO disponiblita (laboratorio_idlaboratorio, orari_idorari) VALUES (?,?)")) {
                for ($iass = 0; $iass < count($elencoOrari); $iass++) {
                    $res->bind_param("ii",$idLaboratorio,$elencoOrari[$iass]);
                    /* execute query */
                    $res->execute();
                }
                /* close statement */
                $res->close();
                if ($res = $this->mysqli->prepare("INSERT INTO abilitazioni (idlaboratorio, idorari, classi_nomeclasse) VALUES (?,?,?)"))
                {
                    for ($ismae = 0; $ismae < count($elencoOrariForClassi); $ismae++) {
                        if (!$res->bind_param("iis",$idLaboratorio,$elencoOrariForClassi[$ismae],$elencoClassiForClassi[$ismae])) {
                            databaseError($res->errno,$res->error);
                        }
                        /* execute query */
                        if (!$res->execute()) {
                            databaseError($res->errno,$res->error);
                        }
                    }
                    /* close statement */
                    $res->close();
                    return $idLaboratorio;
                }
                else
                {
                    //handle the error here
                    connectionError($this->mysqli->error);
                }
            }
            else
            {
                //handle the error here
                connectionError($this->mysqli->error);
            }
        }
        else
        {
            //handle the error here
            connectionError($this->mysqli->error);
        }
    }
    public function getPeopleWithoutBookings()
    {
        if($res = $this->mysqli->prepare("SELECT utenti.username FROM utenti WHERE utenti.username NOT IN (SELECT DISTINCT scelte.utenti_username FROM scelte) AND utenti.livello = 10"))
        {
            /* execute query */
            $res->execute();
            /* store result */
            $res->store_result();
            if($res->num_rows==0)
            {
                return null;
            }
            else
            {
                $result = array();
                $row = array();
                $res->bind_result($row['utenti.username']);
                while($rom = $res->fetch())
                {
                    $result[] = $row['utenti.username'];
                }
                return $result;
            }
            /* free result */
            $res->free_result();
            /* close statement */
            $res->close();
        }
        else
        {
            //handle the error here
            connectionError($this->mysqli->error);
        }
    }
    public function GetLaboratorioByProperty($apikey)
    {
        if($res = $this->mysqli->prepare("SELECT DISTINCT laboratorio.idlaboratorio, laboratorio.nomeLaboratorio, laboratorio.descrizione, laboratorio.aula, laboratorio.limite, laboratorio.durata, disponiblita.orari_idorari, orari.orarioinizio, orari.orariofine FROM proprietari, utenti, accesslist, laboratorio, disponiblita, orari WHERE laboratorio.idlaboratorio = proprietari.laboratorio_id AND accesslist.utenti_username = proprietari.utenti_username AND accesslist.apikey = ? AND disponiblita.laboratorio_idlaboratorio = laboratorio.idlaboratorio AND orari.idorari = disponiblita.orari_idorari ORDER BY orari.orarioinizio"))
        {
            $res->bind_param("s",$apikey);
            /* execute query */
            $res->execute();
            /* store result */
            $res->store_result();
            if($res->num_rows==0)
            {
                return null;
            }
            else
            {
                $result = array();
                $row = array();
                $resultData = null;
                $res->bind_result($row['laboratorio.idlaboratorio'], $row['laboratorio.nomeLaboratorio'], $row['laboratorio.descrizione'], $row['laboratorio.aula'], $row['laboratorio.limite'], $row['laboratorio.durata'], $row['disponiblita.orari_idorari'], $row['orari.orarioinizio'], $row['orari.orariofine']);
                while($rom = $res->fetch())
                {
                    if($resultData==null)
                    {
                        $resultData = new singlelab($row['laboratorio.idlaboratorio'],$row['laboratorio.nomeLaboratorio'],$row['laboratorio.descrizione'],$row['laboratorio.aula'],$row['laboratorio.limite'],$row['laboratorio.durata']);
                    }
                    $resultData->addDisponibilita($row['disponiblita.orari_idorari'], $row['orari.orarioinizio'], $row['orari.orariofine']);
                }
                return $resultData;
            }
            /* free result */
            $res->free_result();
            /* close statement */
            $res->close();
        }
        else
        {
            //handle the error here
            connectionError($this->mysqli->error);
        }
    }
}

?>