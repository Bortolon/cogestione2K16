<?php
class bookingsOp {
    public static function addBookings($args) {
        require_once("returnClass/standard.php");
        require_once("returnClass/laboratori.php");
        require_once("returnClass/orari.php");
        $database = new db();
        $result = $database->checkApiKey($_GET['apikey']);
        if($result!=null)
        {
            if($result['livello']==10)
            {
                if(isset($_POST['arrayLaboratori'])&isset($_POST['arrayFasce']))
                {
                    $arrayLaboratori = $_POST['arrayLaboratori'];
                    $arrayFasce = $_POST['arrayFasce'];
                    //This is a secure mechanism if someone bypass the website interface and send directly the request to web server
                    for($i = 0; $i < count($arrayLaboratori); $i++)
                    {
                        for($d = 0; $d < count($arrayLaboratori); $d++)
                        {
                            if($arrayLaboratori[$i]===$arrayLaboratori[$d] && $i!=$d)
                            {
                                echo json_encode(new Standard("Invalid pass parameter data"));
                                die();
                            }
                        }
                    }
                    for($i = 0; $i < count($arrayFasce); $i++)
                    {
                        for($d = 0; $d < count($arrayFasce); $d++)
                        {
                            if($arrayFasce[$i]===$arrayFasce[$d] && $i!=$d)
                            {
                                echo json_encode(new Standard("Invalid pass parameter data"));
                                die();
                            }
                        }
                    }
                    $laboratori = $database->getLaboratori($result['username']);
                    if($laboratori!=null)
                    {
                        $problematici = array();
                        $countSet = 0;
                        for($i = 0; $i < count($laboratori); $i++)
                        {
                            for($p = 0; $p < count($arrayLaboratori); $p++)
                            {
                                if($laboratori[$i]->idlaboratorio==$arrayLaboratori[$p])
                                {
                                    for($c = 0; $c < count($laboratori[$i]->orari);$c++)
                                    {
                                        if($laboratori[$i]->orari[$c]->mancanti<=0&&$laboratori[$i]->orari[$c]->idOrario===$arrayFasce[$p])
                                        {
                                            $problematici[$countSet]['idLaboratorio'] = $laboratori[$i]->idlaboratorio;
                                            $problematici[$countSet]['idOra'] = $arrayFasce[$p];
                                            $countSet++;
                                        }
                                    }
                                }
                            }
                        }
                        if(count($problematici)===0)
                        {
                            $database->UpdateBookings($result['username'], $arrayLaboratori,$arrayFasce);
                            echo json_encode(new Standard("OK"));
                        }
                        else
                        {
                            require_once("returnClass/bookings.php");
                            echo json_encode(new BookingProblematicCaseResult('Problematic Case',array_values($problematici)));
                        }
                    }
                }
                else
                {
                    echo json_encode(new Standard("Require parameter is not set"));
                }
            }
            else
            {
                echo json_encode(new Standard("Forbidden operation for this user level"));
            }
        }
        else
        {
            require_once("returnClass/standard.php");
            echo json_encode(new Standard("Api Key is not valid"));
        }
    }
    public static function getBookedStudents($args) {
        require_once("returnClass/bookings.php");
        require_once("returnClass/studenti.php");
        $database = new db();
        $result = $database->checkApiKey($_GET['apikey']);
        if($result!=null)
        {
            if($result['livello']===7 || $result['livello']===1)
            {
                $labProp = $database->GetLaboratorioByProperty($_GET['apikey']);
                if($result['livello']===1||($result!=NULL ? $labProp->idLaboratorio===$args['idLaboratorio'] : false))
                {
                    $idLaboratorio = $args['idLaboratorio'];
                    $idOrario = $args['idLaboratorioFascia'];
                    $result = $database->GetStudents($idLaboratorio, $idOrario);
                    echo json_encode(new BookedStudentsResult("OK",$result));
                }
                else
                {
                    require_once("returnClass/standard.php");
                    echo json_encode(new Standard("Forbidden operation for this user"));
                }
            }
            else
            {
                require_once("returnClass/standard.php");
                echo json_encode(new Standard("Forbidden operation for this user level"));
            }
        }
        else
        {
            require_once("returnClass/standard.php");
            echo json_encode(new Standard("Api Key is not valid"));
        }
    }
}
?>