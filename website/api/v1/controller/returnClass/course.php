<?php
class CourseResult implements JsonSerializable {
    public $message = "";
    public $laboratori = array();
    public $orari = array();
    public $scelte = null;
    public function __construct($message, $orari, $laboratori, $scelte) {
        $this->message = $message;
        $this->laboratori = $laboratori;
        $this->orari = $orari;
        $this->scelte = $scelte;
    }
    public function jsonSerialize() {
        return ['message' => $this->message, 'orari' => $this->orari, 'laboratori' => $this->laboratori, 'scelte' => $this->scelte];
    }
}

class CourseCompleteResult implements JsonSerializable {
    public $message = "";
    public $laboratori = array();
    public $orari = array();
    public $classi = null;
    public function __construct($message, $orari, $laboratori, $classi) {
        $this->message = $message;
        $this->laboratori = $laboratori;
        $this->orari = $orari;
        $this->classi = $classi;
    }
    public function jsonSerialize() {
        return ['message' => $this->message, 'orari' => $this->orari, 'laboratori' => $this->laboratori, 'classi' => $this->classi];
    }
}
class CourseDetailResult implements JsonSerializable {
    public $message = "";
    public $laboratorio = null;
    public $orari = array();
    public $stringSingle = array();
    public function __construct($message, $orari, $laboratorio) {
        $this->message = $message;
        $this->laboratori = $laboratorio;
        $this->orari = $orari;
    }
    public function jsonSerialize() {
        return ['message' => $this->message, 'orari' => $this->orari, 'laboratorio' => $this->laboratori];
    }
}
?>