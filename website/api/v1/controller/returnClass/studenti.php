<?php
class studenti implements JsonSerializable {
    public $nome = "";
    public $cognome = "";
    public $classe = "";
    public function __construct($nome, $cognome, $classe) {
        $this->nome = $nome;
        $this->cognome = $cognome;
        $this->classe = $classe;
    }
    public function jsonSerialize() {
        return ['nome' => $this->nome, 'cognome' => $this->cognome, 'classe' => $this->classe];
    }
}
?>