<?php
    class scelte implements JsonSerializable {
    public $idOrario = 0;
    public $idLaboratorio =0;
    public function __construct($idOrario, $idLaboratorio) {
        $this->idOrario = $idOrario;
        $this->idLaboratorio = $idLaboratorio;
    }
    public function jsonSerialize() {
        return ['idOrario' => $this->idOrario, 'idLaboratorio' => $this->idLaboratorio];
    }
}
?>