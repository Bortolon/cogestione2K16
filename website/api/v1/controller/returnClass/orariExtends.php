<?php
class orariExtends extends orari {
    public $classi = array();
    public function addClasse($classe)
    {
        $this->classi[] = $classe;
    }
    public function jsonSerialize() {
        return ['idOrario' => $this->idOrario, 'mancanti' => $this->mancanti, 'classi' => $this->classi];
    }
}
?>