<?php
class orari implements JsonSerializable
{
    public $idOrario = 0;
    public $mancanti = 0;
    public function __construct($idOrario, $mancanti) {
        $this->idOrario = $idOrario;
        $this->mancanti = $mancanti;
    }
    public function jsonSerialize() {
        return ['idOrario' => $this->idOrario, 'mancanti' => $this->mancanti];
    }
}
?>