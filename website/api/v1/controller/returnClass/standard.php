<?php
class Standard implements JsonSerializable {
    public $message = "";
    public function __construct($message) {
        $this->message = $message;
    }
    public function jsonSerialize() {
        return ['message' => $this->message];
    }
}