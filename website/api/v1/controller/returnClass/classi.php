<?php
class classi implements JsonSerializable {
    public $classe = "";
    public $categoria = "";
    public function __construct($classe, $categoria) {
        $this->classe = $classe;
        $this->categoria = $categoria;
    }
    public function jsonSerialize() {
        return ['classe' => $this->classe, 'categoria' => $this->categoria];
    }
}
?>