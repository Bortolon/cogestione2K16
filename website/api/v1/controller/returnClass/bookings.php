<?php

class BookedStudentsResult implements JsonSerializable {
    public $message = "";
    public $studenti = "";
    public function __construct($message, $studenti) {
        $this->message = $message;
        $this->studenti = $studenti;
    }
    public function jsonSerialize() {
        return ['message' => $this->message, 'studenti' => $this->studenti];
    }
}

class BookingProblematicCaseResult implements JsonSerializable {
    public $message = "";
    public $problematicCase = "";
    public function __construct($message, $problematicCase) {
        $this->message = $message;
        $this->problematicCase = $problematicCase;
    }
    public function jsonSerialize() {
        return ['message' => $this->message, 'problematic' => $this->problematicCase];
    }
}

?>