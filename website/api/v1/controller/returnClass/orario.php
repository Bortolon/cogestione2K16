<?php
class orario implements JsonSerializable {
    public $nomeLaboratorio = "";
    public $oraInizio = "";
    public $aula = "";
    public $descrizione = "";
    public function __construct($nomeLaboratorio, $descrizione, $aula, $oraInizio, $durata) {
        $this->nomeLaboratorio = $nomeLaboratorio;
        $this->descrizione = $descrizione;
        $this->aula = $aula;
        $this->oraInizio = $oraInizio;
        $this->durata = $durata;
    }
    public function jsonSerialize() {
        return ['nomeLaboratorio' => $this->nomeLaboratorio, 'descrizione' => $this->descrizione, 'aula' => $this->aula, 'oraInizio' => $this->oraInizio, 'durata' => $this->durata];
    }
}
?>