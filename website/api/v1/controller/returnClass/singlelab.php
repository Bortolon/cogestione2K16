<?php
class singlelab implements JsonSerializable {
    public $idLaboratorio = -1;
    public $nomeLaboratorio = "";
    public $descrizione = "";
    public $aula = "";
    public $limite = 0;
    public $durata = 0;
    public $disponibilita = array();
    public function __construct($idLaboratorio, $nomeLaboratorio, $descrizione, $aula, $limite, $durata) {
        $this->idLaboratorio = $idLaboratorio;
        $this->nomeLaboratorio = $nomeLaboratorio;
        $this->descrizione = $descrizione;
        $this->aula = $aula;
        $this->limite = $limite;
        $this->durata = $durata;
    }
    public function addDisponibilita($idOrario, $dataInizio, $dataFine) {
        $this->disponibilita[] = ['idOrario' => $idOrario, 'dataInizio' => $dataInizio, 'dataFine' => $dataFine];
    }
    public function jsonSerialize() {
        return ['idLaboratorio' => $this->idLaboratorio, 'nomeLaboratorio' => $this->nomeLaboratorio, 'descrizione' => $this->descrizione, 'aula' => $this->aula, 'limite' => $this->limite, 'durata' => $this->durata, 'disponibilita' => $this->disponibilita];
    }
}
?>