<?php
class laboratori implements JsonSerializable {
    public $idlaboratorio = 0;
    public $nomeLaboratorio = "";
    public $descrizione = "";
    public $aula = NULL;
    public $limite = 0;
    public $durata = 0;
    public $orari = array();
    public $extendedOrari = false;
    public function __construct($idlaboratorio, $nomeLaboratorio, $descrizione, $limite, $durata) {
        $this->idlaboratorio = $idlaboratorio;
        $this->nomeLaboratorio = $nomeLaboratorio;
        $this->descrizione = $descrizione;
        $this->limite = $limite;
        $this->durata = $durata;
    }
    public function setAula($aula)
    {
        $this->aula = $aula;
    }
    public function addOrari($idorari, $mancanti)
    {
        if($this->extendedOrari===true)
        {
            $this->orari[$idorari] = new orariExtends($idorari, $mancanti);
        }
        else
        {
            $this->orari[] = new orari($idorari, $mancanti);
        }
    }
    public function addClasse($idorari, $classe)
    {
        if($this->extendedOrari===true)
        {
            $this->orari[$idorari]->addClasse($classe);
        }
    }
    public function setExtendedOrari($boolean)
    {
        $this->extendedOrari = $boolean;
    }
    public function jsonSerialize() {
        if($this->extendedOrari===true)
        {
            $this->orari = array_values($this->orari);
        }
        $returnArra = ['idlaboratorio' => $this->idlaboratorio, 'nomeLaboratorio' => $this->nomeLaboratorio, 'descrizione' => $this->descrizione, 'limite' => $this->limite, 'durata' => $this->durata, 'orariQuestoLab' => $this->orari ];
        if($this->aula!=NULL)
        {
            $returnArra['aula'] = $this->aula;
        }
        return $returnArra;
    }
}
class LaboratorioDetailResult implements JsonSerializable {
    public $message = "";
    public $laboratorio = "";
    public function __construct($message, $laboratorio) {
        $this->message = $message;
        $this->laboratorio = $laboratorio;
    }
    public function jsonSerialize() {
        return ['message' => $this->message, 'laboratorio' => $this->laboratorio];
    }
}
?>