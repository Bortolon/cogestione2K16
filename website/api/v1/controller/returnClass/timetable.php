<?php
class TimetableResult implements JsonSerializable {
    public $message = "";
    public $orari = "";
    public function __construct($message, $orari) {
        $this->message = $message;
        $this->orari = $orari;
    }
    public function jsonSerialize() {
        return ['message' => $this->message, 'orari' => $this->orari ];
    }
}
?>