<?php
class LoginResult implements JsonSerializable {
    public $message = "";
    public $redirectAddress = "";
    public $apiKey = "";
    public $displayName = "";
    public function __construct($message, $apikey, $redirectAddress, $displayName) {
        $this->message = $message;
        $this->apikey = $apikey;
        $this->redirectAddress = $redirectAddress;
        $this->displayName = $displayName;
    }
    public function jsonSerialize() {
        return ['message' => $this->message, 'redirectAddress' => $this->redirectAddress, 'apiKey' => $this->apikey, 'displayName' => $this->displayName ];
    }
}
?>