<?php
class classiOp {
    public static function getClassi($args) {
        require_once('returnClass/classi.php');
        $database = new db();
        $result = $database->checkApiKey($_GET['apikey']);
        if($result!=null)
        {
            if($result['livello']==1)
            {
                $result = $database->GetClassi();
                if($result!=null)
                {
                    echo json_encode(["message"=>"OK","classi"=>$result]);
                }
                else
                {
                    echo "{\"message\":\"Classi not insert in the system\"}";
                }
            }
            else
            {
                echo "{\"message\":\"Forbidden operation for this user level\"}";
            }
        }
        else
        {
            echo "{\"message\":\"Api Key is not valid\"}";
        }
    }
    public static function insertClassi($args) {
        $database = new db();
        $result = $database->checkApiKey($_GET['apikey']);
        if($result!=null)
        {
            if($result['livello']==1)
            {
                if(isset($_POST["classe"])&&isset($_POST["categoria"]))
                {
                    $result = $database->InsertClassi($_POST["classe"],$_POST["categoria"]);
                    echo json_encode(["message"=>"OK"]);
                }
                else
                {
                    echo "{\"message\":\"Require parameter is not set\"}";
                }
            }
            else
            {
                echo "{\"message\":\"Forbidden operation for this user level\"}";
            }
        }
        else
        {
            echo "{\"message\":\"Api Key is not valid\"}";
        }
    }
    public static function updateClassi($args) {
        parse_str(file_get_contents("php://input"),$put);
        $database = new db();
        $result = $database->checkApiKey($_GET['apikey']);
        if($result!=null)
        {
            if($result['livello']==1)
            {
                if(isset($put["oldClasse"])&&isset($put["newClasse"])&&isset($put["categoria"]))
                {
                    $result = $database->UpdateClasse($put["oldClasse"],$put["newClasse"],$put["categoria"]);
                    echo json_encode(["message"=>"OK"]);
                }
                else
                {
                    echo "{\"message\":\"Require parameter is not set\"}";
                }
            }
            else
            {
                echo "{\"message\":\"Forbidden operation for this user level\"}";
            }
        }
        else
        {
            echo "{\"message\":\"Api Key is not valid\"}";
        }
    }
}
?>