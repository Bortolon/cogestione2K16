<?php

class timetableOp {
    public static function gettimetable($args) {
        $database = new db();
        $result = $database->checkApiKey($_GET['apikey']);
        if($result!=null)
        {
            if($result['livello']==10)
            {
                $result = $database->GetTimetable($result['username']);
                if($result!=null) {
                    echo json_encode(new TimetableResult("OK",$result));
                }
                else
                {
                    require_once("returnClass/standard.php");
                    echo json_encode(new Standard("No timetable set"));
                }
            }
            else
            {
                require_once("returnClass/standard.php");
                echo json_encode(new Standard("Forbidden operation for this user level"));
            }
        }
        else
        {
            require_once("returnClass/standard.php");
            echo json_encode(new Standard("Api Key is not valid"));
        }
    }
}
?>