<?php

require_once("returnClass/login.php");

class loginOp {
    public static function obtainApiKey($args) {
        $database = new db();
        $result = $database->obtainLoginUserInfo($_POST['username'],$_POST['password']);
        if($result!=null)
        {
            $apiKey = $database->insertApiKey($result['username']);
            $redirectAddress = "http://cogestione.barsanti.gov.it/index.html";
            if($result['livello'] == 1) {
                $redirectAddress = "http://cogestione.barsanti.gov.it/admin.html?apikey=".$apiKey;
            }
            else if($result['livello'] == 10) {
                $ini = parse_ini_file('config-www/access.ini');
                if($ini['stage']==="booking")
                {
                    $redirectAddress = "http://cogestione.barsanti.gov.it/booking.html?apikey=".$apiKey;
                }
                else if($ini['stage']==="timetable")
                {
                    $redirectAddress = "http://cogestione.barsanti.gov.it/studentTime.html?apikey=".$apiKey;
                }
                else
                {
                    $redirectAddress = "http://cogestione.barsanti.gov.it/close.html";
                    $database->deleteApiKey($apiKey);
                }
            }
            else if($result['livello'] == 7) {
                $redirectAddress = "http://cogestione.barsanti.gov.it/laboratorioTime.html?apikey=".$apiKey;
            }
            else if($result['livello'] == 8) {
                $redirectAddress = "http://www.cogestione.barsanti.gov.it/teacher.html?apikey=".$apiKey;
            }
            echo json_encode(new LoginResult("OK",$apiKey,$redirectAddress,$result['display']));
        }
        else
        {
            require_once('returnClass/standard.php');
            echo json_encode(new Standard("User not found"));
        }
    }
    public static function logout($args) {
        require_once('returnClass/standard.php');
        $database = new db();
        $result = $database->deleteApiKey($_GET['apikey']);
        echo json_encode(new Standard("OK"));
    }
}
?>
