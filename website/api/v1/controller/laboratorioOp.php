<?php
class laboratorioOp {
    public static function addLaboratorio($args) {
        $database = new db();
        $result = $database->checkApiKey($_GET['apikey']);
        if($result!=null)
        {
            if($result['livello']==1)
            { if(isset($_POST['name'])&&isset($_POST['description'])&&isset($_POST['classroom'])&&isset($_POST['duration'])&&isset($_POST['limit'])&&isset($_POST['hours'])&&isset($_POST['enabledClassHours'])&&isset($_POST['enabledClass']))
                {
                    $id = null;
                    if(isset($_POST['id']))
                    {
                        $id = $_POST['id'];
                    }
                    $name = $_POST['name'];
                    $description = $_POST['description'];
                    $duration = $_POST['duration'];
                    $classroom = $_POST['classroom'];
                    $limit = $_POST['limit'];
                    $hours = $_POST['hours'];
                    $enabledClassHours = $_POST['enabledClassHours'];
                    $enabledClass = $_POST['enabledClass'];
                    $idLab = $database->addLaboratorio($id, $name, $description, $classroom, $limit, $duration, $hours, $enabledClassHours, $enabledClass);
                    echo "{\"message\":\"OK\",\"idLaboratorio\":".$idLab."}";
                }
                else
                {
                    echo "{\"message\":\"Require parameter is not set\"}";
                }
            }
            else
            {
                echo "{\"message\":\"Forbidden operation for this user level\"}";
            }
        }
        else
        {
            echo "{\"message\":\"Api Key is not valid\"}";
        }
    }

    public static function assignedLaboratorio($args) {
        require_once("returnClass/laboratori.php");
        require_once("returnClass/singlelab.php");
        $database = new db();
        $result = $database->checkApiKey($_GET['apikey']);
        if($result!=null)
        {
            if($result['livello']==7)
            {
                $result = $database->GetLaboratorioByProperty($_GET['apikey']);
                echo json_encode(new LaboratorioDetailResult("OK",$result));
            }
            else
            {
                require_once("returnClass/standard.php");
                echo json_encode(["message"=>"Forbidden operation for this user level"]);
            }
        }
        else
        {
            require_once("returnClass/standard.php");
            echo json_encode(["message"=>"Api Key is not valid"]);
        }
    }
}
?>