<?php

class courseOp {
    public static function getCourse($args) {
        require_once('returnClass/course.php');
        require_once('returnClass/laboratori.php');
        require_once('returnClass/orari.php');
        require_once('returnClass/classi.php');
        require_once('returnClass/scelte.php');
        $database = new db();
        $result = $database->checkApiKey($_GET['apikey']);
        if($result!=null)
        {
            $ore = $database->getHours();
            if($ore!=null)
            {
                $laborator = null;
                $scelte = null;
                if($result['livello']!=1 && $result['livello']!=7)
                {
                    $laborator = $database->getLaboratori($result['username']);
                    $scelte = $database->GetScelte($result['username']);
                }
                else
                {
                    $laborator = $database->getAllLaboratori();
                }
                if($result['livello']==1)
                {
                    $classi = $database->GetClassi();
                    if($classi!=null)
                    {
                        echo json_encode(new CourseCompleteResult("OK",$ore,$laborator,$classi));
                    }
                    else
                    {
                        echo "{\"message\":\"Classi not insert in the system\"}";
                    }
                }
                else if($laborator!=null) {
                    echo json_encode(new CourseResult("OK",$ore,$laborator,$scelte));
                }
                else
                {
                    echo "{\"message\":\"Laboratori not insert for this user\"}";
                }
            }
            else
            {
                echo "{\"message\":\"No hours in the system\"}";
            }
        }
        else
        {
            echo "{\"message\":\"Api Key is not valid\"}";
        }
    }
    public static function getDetailCourse($args) {
        require_once('returnClass/course.php');
        require_once('returnClass/laboratori.php');
        require_once('returnClass/orari.php');
        require_once('returnClass/orariExtends.php');
        require_once('returnClass/classi.php');
        $database = new db();
        $result = $database->checkApiKey($_GET['apikey']);
        if($result!=null)
        {
            $ore = $database->getHours();
            if($ore!=null)
            {
                $classi = $database->GetClassi();
                if($classi!=null)
                {
                    if(isset($args['idLaboratorio']))
                    {
                        $laboratorio = $database->LaboratorioDetailed($args['idLaboratorio']);
                        if($laboratorio!=null)
                        {
                            echo json_encode(new CourseDetailResult("OK",$ore, $laboratorio));
                        }
                        else
                        {
                            echo "{\"message\":\"Laboratorio id not correct\"}";
                        }
                    }
                    else
                    {
                        echo "{\"message\":\"Parameter not correct\"}";
                    }
                }
                else
                {
                    echo "{\"message\":\"No classi in the system\"}";
                }
            }
            else
            {
                echo "{\"message\":\"No hours in the system\"}";
                
            }
        }
        else
        {
            echo "{\"message\":\"Api Key is not valid\"}";
        }
    }
}
?>