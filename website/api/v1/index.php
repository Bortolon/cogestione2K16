<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 'on');
    require_once('AltoRouter.php');
    require_once("db.php");
    $router = new AltoRouter();
    $router->setBasePath('/api/v1');
    $router->map('POST','/login', 'loginOp#obtainApiKey', 'login');
    $router->map('DELETE','/login', 'loginOp#logout', 'logout');
    
    $router->map('POST','/booking', 'bookingsOp#addBookings','addBookings');
    $router->map('GET','/bookedstudent/[i:idLaboratorio]/[i:idLaboratorioFascia]','bookingsOp#getBookedStudents','getBookedStudents');

    $router->map('GET','/class', 'classiOp#getClassi','getClassi');
    $router->map('POST','/class','classiOp#insertClassi','insertClassi');
    $router->map('PUT','/class','classiOp#updateClassi','updateClassi');

    $router->map('GET','/course','courseOp#getCourse','getCourse');
    $router->map('GET','/coursedetail/[i:idLaboratorio]','courseOp#getDetailCourse','getDetailCourse');
    
    $router->map('POST','/laboratory','laboratorioOp#addLaboratorio','addLaboratorio');
    $router->map('GET','/assignedlab','laboratorioOp#assignedLaboratorio','assignedLaboratorio');

    //TODO: add function for set remove and edit owner account of lab

    //TODO: add function for set remove and edit student account

    //TODO: add function to reset the password and optional change it

    $match = $router->match();
    if($match) {
        list( $controller, $action ) = explode( '#', $match['target'] );
        require_once("controller/$controller.php");
        if ( is_callable(array($controller, $action)) ) {
            call_user_func_array(array($controller,$action), array($match['params']));
        } else {
            header("HTTP/1.0 505 Internal Server Error");
            echo json_encode(["message"=>"505 internal server error"]);
            // here your routes are wrong.
            // Throw an exception in debug, send a  500 error in production
        }
    } else {
        // no route was matched
        header("HTTP/1.0 404 Not Found");
        echo json_encode(["message"=>"404 page not found"]);
    }
?>