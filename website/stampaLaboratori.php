<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 'on');
    spl_autoload_register(function ($className) {
        require_once 'api/v1/' . $className . '.php';
    });
    $database = new db();
    $laborator = null;
    $ore = null;
    $result = $database->checkApiKey($_GET['apikey']);
    if($result!=null)
    {
        $ore = $database->getHours();
        if($ore!=null)
        {
            $laborator = $database->getAllLaboratori();
        }
        else
        {
            die("TURNI ERROR");
        }
    }
    else
    {
        die("API KEY ERROR");
    }
?>
<html>
    <head>
        <title>Cogestione - Stampa Elenco Laboratori</title>
        <link rel="stylesheet" type="text/css" href="css/stampaLaboratori.css" />
    </head>
    <body>
        <?php
        for($i = 0;$i < count($laborator); $i++)
        {
            $nome = $laborator[$i]->nomeLaboratorio;
            $responsabiliLaboratorio = "";
            $responsabili = $database->getResponsabili($laborator[$i]->idlaboratorio);
            $requireVirgola = false;
            echo count($responsabili);
            for($d = 0;$d < count($responsabili);$d++)
            {
                if($requireVirgola===true)
                {
                    $responsabiliLaboratorio = $responsabiliLaboratorio . "," . $responsabili[$d]['nome'] . ' ' . $responsabili[$d]['cognome'];
                }
                else
                {
                    $responsabiliLaboratorio = $responsabiliLaboratorio . "" . $responsabili[$d]['nome'] . ' ' . $responsabili[$d]['cognome'];
                    $requireVirgola = true;
                }
            }
            for($d = 0;$d < count($laborator[$i]->orari);$d++)
            {
                $foundTurnName = "";
                $idTurno = $laborator[$i]->orari[$d]->idOrario;
                for($e = 0;$e < count($ore);$e++)
                {
                    if($idTurno === $ore[$e]['idorari'])
                    {
                        $foundTurnName = $ore[$e]['orarioinizio'];
                    }
                }
                echo "<div class=\"lab-print\"><div class=\"informazioni-head\">Nome Laboratorio:</div><div class=\"informazioni-body\">".$nome."</div><br/><div class=\"informazioni-head\">Orario inizio turno:</div><div class=\"informazioni-body\">".$foundTurnName."</div><div class=\"informazioni-head\">Responsabili:</div><div class=\"informazioni-body\">".$responsabiliLaboratorio."</div>";
                echo "<table class=\"student-list\"><thead><tr><th>N°:</th><th>Nome:</th><th>Cognome:</th><th>Classe:</th></tr></thead><tbody>";
                $studenti = $database->GetStudents($laborator[$i]->idlaboratorio, $idTurno);
                for($e = 0;$e < count($studenti);$e++)
                {
                    echo "<tr><td>".($e+1)."</td><td>".$studenti[$e]->nome."</td><td>".$studenti[$e]->cognome."</td><td>".$studenti[$e]->classe."</td></tr>";
                }
                echo "</tbody></table></div>";
            }
        }
        ?>
    </body>
</html>