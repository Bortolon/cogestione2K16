SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE DATABASE IF NOT EXISTS `cogestione` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `cogestione`;

CREATE TABLE IF NOT EXISTS `abilitazioni` (
  `idlaboratorio` int(11) NOT NULL,
  `idorari` int(11) NOT NULL,
  `classi_nomeclasse` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `accesslist` (
  `apikey` varchar(130) NOT NULL,
  `ultimaoperazione` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `utenti_username` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `classi` (
  `nomeclasse` varchar(5) NOT NULL,
  `categoria` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `disponiblita` (
  `laboratorio_idlaboratorio` int(11) NOT NULL,
  `orari_idorari` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `laboratorio` (
  `idlaboratorio` int(11) NOT NULL,
  `nomeLaboratorio` varchar(50) NOT NULL,
  `descrizione` varchar(100) NOT NULL,
  `aula` varchar(50) NOT NULL,
  `limite` int(11) NOT NULL,
  `durata` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `orari` (
  `idorari` int(11) NOT NULL,
  `orarioinizio` datetime NOT NULL,
  `orariofine` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `preferenze` (
  `laboratorio_idlaboratorio` int(11) NOT NULL,
  `utenti_username` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `proprietari` (
  `utenti_username` varchar(50) NOT NULL,
  `laboratorio_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `scelte` (
  `utenti_username` varchar(50) NOT NULL,
  `laboratorio_has_orari_laboratorio_idlaboratorio` int(11) NOT NULL,
  `laboratorio_has_orari_orari_idorari` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `utenti` (
  `username` varchar(50) NOT NULL,
  `password` varchar(130) NOT NULL,
  `livello` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `cognome` varchar(50) NOT NULL,
  `classi_nomeclasse` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `utenti` (`username`, `password`, `livello`, `nome`, `cognome`, `classi_nomeclasse`) VALUES
('admin', 'e64b65245ba01a0a5b1f9a7172edff6b9de41335381d467ae6dcd969cb97a41836f658b0badbe29a436949ad1566c288536c6072c24c1fd0db5cf7f46f0e3bdb', 1, 'Admin', 'Admin', NULL);

ALTER TABLE `abilitazioni`
  ADD PRIMARY KEY (`idlaboratorio`,`idorari`,`classi_nomeclasse`),
  ADD KEY `fk_laboratorio_has_orari_has_classi_classi1_idx` (`classi_nomeclasse`),
  ADD KEY `fk_laboratorio_has_orari_has_classi_laboratorio_has_orari1_idx` (`idlaboratorio`,`idorari`);

ALTER TABLE `accesslist`
  ADD PRIMARY KEY (`apikey`),
  ADD KEY `fk_accesslist_utenti1_idx` (`utenti_username`);

ALTER TABLE `classi`
  ADD PRIMARY KEY (`nomeclasse`);

ALTER TABLE `disponiblita`
  ADD PRIMARY KEY (`laboratorio_idlaboratorio`,`orari_idorari`),
  ADD KEY `fk_laboratorio_has_orari_orari1_idx` (`orari_idorari`),
  ADD KEY `fk_laboratorio_has_orari_laboratorio1_idx` (`laboratorio_idlaboratorio`);

ALTER TABLE `laboratorio`
  ADD PRIMARY KEY (`idlaboratorio`);

ALTER TABLE `orari`
  ADD PRIMARY KEY (`idorari`);

ALTER TABLE `preferenze`
  ADD PRIMARY KEY (`laboratorio_idlaboratorio`,`utenti_username`),
  ADD KEY `fk_laboratorio_has_utenti_utenti1_idx` (`utenti_username`),
  ADD KEY `fk_laboratorio_has_utenti_laboratorio1_idx` (`laboratorio_idlaboratorio`);

ALTER TABLE `proprietari`
  ADD PRIMARY KEY (`utenti_username`,`laboratorio_id`),
  ADD KEY `fk_laboratorio` (`laboratorio_id`);

ALTER TABLE `scelte`
  ADD PRIMARY KEY (`utenti_username`,`laboratorio_has_orari_laboratorio_idlaboratorio`,`laboratorio_has_orari_orari_idorari`),
  ADD KEY `fk_utenti_has_laboratorio_has_orari_laboratorio_has_orari1_idx` (`laboratorio_has_orari_laboratorio_idlaboratorio`,`laboratorio_has_orari_orari_idorari`),
  ADD KEY `fk_utenti_has_laboratorio_has_orari_utenti1_idx` (`utenti_username`);

ALTER TABLE `utenti`
  ADD PRIMARY KEY (`username`),
  ADD KEY `fk_utenti_classi_idx` (`classi_nomeclasse`);


ALTER TABLE `laboratorio`
  MODIFY `idlaboratorio` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
ALTER TABLE `orari`
  MODIFY `idorari` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;

ALTER TABLE `abilitazioni`
  ADD CONSTRAINT `fk_disponibilita` FOREIGN KEY (`idlaboratorio`, `idorari`) REFERENCES `disponiblita` (`laboratorio_idlaboratorio`, `orari_idorari`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_classi` FOREIGN KEY (`classi_nomeclasse`) REFERENCES `classi` (`nomeclasse`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `accesslist`
  ADD CONSTRAINT `fk_accesslist_utenti1` FOREIGN KEY (`utenti_username`) REFERENCES `utenti` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `disponiblita`
  ADD CONSTRAINT `fk_laboratorio_has_orari_laboratorio1` FOREIGN KEY (`laboratorio_idlaboratorio`) REFERENCES `laboratorio` (`idlaboratorio`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_laboratorio_has_orari_orari1` FOREIGN KEY (`orari_idorari`) REFERENCES `orari` (`idorari`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `preferenze`
  ADD CONSTRAINT `fk_laboratorio_has_utenti_laboratorio1` FOREIGN KEY (`laboratorio_idlaboratorio`) REFERENCES `laboratorio` (`idlaboratorio`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_laboratorio_has_utenti_utenti1` FOREIGN KEY (`utenti_username`) REFERENCES `utenti` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `proprietari`
  ADD CONSTRAINT `fk_laboratorio` FOREIGN KEY (`laboratorio_id`) REFERENCES `laboratorio` (`idlaboratorio`),
  ADD CONSTRAINT `fk_username` FOREIGN KEY (`utenti_username`) REFERENCES `utenti` (`username`);

ALTER TABLE `scelte`
  ADD CONSTRAINT `fk_utenti_has_laboratorio_has_orari_laboratorio_has_orari1` FOREIGN KEY (`laboratorio_has_orari_laboratorio_idlaboratorio`, `laboratorio_has_orari_orari_idorari`) REFERENCES `disponiblita` (`laboratorio_idlaboratorio`, `orari_idorari`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_utenti_has_laboratorio_has_orari_utenti1` FOREIGN KEY (`utenti_username`) REFERENCES `utenti` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `utenti`
  ADD CONSTRAINT `fk_utenti_classi` FOREIGN KEY (`classi_nomeclasse`) REFERENCES `classi` (`nomeclasse`) ON DELETE CASCADE ON UPDATE CASCADE;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` EVENT `AccessListDelete` ON SCHEDULE EVERY 1 MINUTE STARTS '2016-03-01 00:00:00' ENDS '2016-04-30 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO DELETE FROM accesslist WHERE TIMESTAMPDIFF(MINUTE,accesslist.ultimaoperazione,CURRENT_TIMESTAMP) > 30$$

DELIMITER ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
