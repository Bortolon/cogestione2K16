-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Mar 21, 2016 at 08:31 AM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cogestione`
--
CREATE DATABASE IF NOT EXISTS `cogestione` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `cogestione`;

--
-- Dumping data for table `abilitazioni`
--

INSERT INTO `abilitazioni` (`laboratorio_has_orari_laboratorio_idlaboratorio`, `laboratorio_has_orari_orari_idorari`, `classi_nomeclasse`) VALUES
(11, 6, '1E'),
(11, 6, '4AI'),
(11, 8, '1E'),
(11, 8, '4AI'),
(12, 6, '1E'),
(12, 6, '4AI'),
(12, 8, '1E'),
(12, 8, '4AI'),
(13, 6, '1E'),
(13, 6, '4AI'),
(13, 7, '1E'),
(13, 7, '4AI'),
(13, 8, '1E'),
(13, 8, '4AI'),
(13, 9, '1E'),
(13, 9, '4AI'),
(13, 10, '1E'),
(13, 10, '4AI'),
(14, 7, '4AI'),
(14, 9, '4AI'),
(15, 6, '4AI'),
(15, 7, '4AI'),
(15, 8, '4AI'),
(15, 9, '4AI'),
(15, 10, '4AI'),
(16, 7, '1E'),
(16, 9, '1E'),
(17, 6, '1E'),
(17, 8, '1E'),
(18, 6, '1E'),
(18, 7, '1E'),
(18, 8, '1E'),
(18, 9, '1E'),
(18, 10, '1E'),
(19, 7, '1E'),
(19, 9, '1E'),
(20, 6, '1E'),
(20, 7, '1E'),
(20, 8, '1E'),
(20, 9, '1E'),
(20, 10, '1E');

--
-- Dumping data for table `disponiblita`
--

INSERT INTO `disponiblita` (`laboratorio_idlaboratorio`, `orari_idorari`) VALUES
(11, 6),
(12, 6),
(13, 6),
(15, 6),
(17, 6),
(18, 6),
(20, 6),
(13, 7),
(14, 7),
(15, 7),
(16, 7),
(18, 7),
(19, 7),
(20, 7),
(11, 8),
(12, 8),
(13, 8),
(15, 8),
(17, 8),
(18, 8),
(20, 8),
(13, 9),
(14, 9),
(15, 9),
(16, 9),
(18, 9),
(19, 9),
(20, 9),
(13, 10),
(15, 10),
(18, 10),
(20, 10);

--
-- Dumping data for table `laboratorio`
--

INSERT INTO `laboratorio` (`idlaboratorio`, `nomeLaboratorio`, `descrizione`, `aula`, `limite`, `durata`) VALUES
(11, 'Cinema', 'Cinema', 'Aula Magna', 60, 2),
(12, 'Modellismo', 'Modellismo', 'Laboratorio Fisica', 30, 2),
(13, 'Fisica dello sport', 'Fisica applicata allo sport', '5AI', 15, 1),
(14, 'Programmazione App Android', 'Programmazione applicazioni Android', 'Informatica 4', 25, 2),
(15, 'Primi passi con Raspberry', 'prime prove con Raspberry', 'Laboratorio Sistemi', 35, 1),
(16, 'Autocad e^x', 'Autocad alla massima potenza', 'Informatica 1', 30, 2),
(17, 'Saldatura', 'Saldatura ad arco', 'Lab Tecnologico', 30, 2),
(18, 'Fratture dei metalli', 'Come riconoscere e analizare le fratture metalliche', '3AI', 35, 1),
(19, 'Primo Soccorso', 'Laboratorio di primo soccorso', 'Info 2', 30, 2),
(20, 'CNC', 'Spiegazione del CNC', 'Lab Meccanica', 30, 1);

--
-- Dumping data for table `orari`
--

INSERT INTO `orari` (`idorari`, `orarioinizio`, `orariofine`) VALUES
(6, '2016-04-10 08:00:00', '2016-04-10 09:00:00'),
(7, '2016-04-10 09:00:00', '2016-04-10 10:00:00'),
(8, '2016-04-10 10:00:00', '2016-04-10 11:00:00'),
(9, '2016-04-10 11:00:00', '2016-04-10 12:00:00'),
(10, '2016-04-10 12:00:00', '2016-04-10 13:00:00');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
