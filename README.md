Cogestione 2K16
===============

Installation:  
-------------

1) Download the project or download by github.com  
2) Execute the SQL file database/cogestioneDebug.sql (for debug purpose) or database/cogestioneDb.sql (for installation purpose)  
3) Create a new virtualhost on your system that use the website folder (recommended)  

The project structure:  
----------------------

The project is based on the concept of WWW 2.0 so for relationship with the DB we have to make every time a AJAX call and base on this respone we do the appropriate action  

```  
|-Cogestione2K16.postman_collection.json contain the list of possible API call  
|-Documentazione Cogestione 2K16.docx contain an extract of API call (useful to create external application as an Android API) (unrefresh)  
|-funzioniApiElenco.xlsx contain list of API call developed or we'll develop in the future  
|-database  
    |-cogestioneDb.sql contain the installation SQL instruction for a production enviroment  
    |-inserimento*.sql contain some script to demostrate the manual insert of data inside database (used in the past year)  
    |-queryFatte.txt contain a list of some query that became useful past year  
    |-cogestioneDebug.sql contain the installation SQL instruction for a debug enviroment (with some sample data)  
|-password contain the list of password created last year (we have must to change the password font because the last year was a suicide)  
|-website  
    |-admin.html it's the html used for the admin webpage load  
    |-booking.html it's used to permit to people of booking  
    |-close.html it's used when the system isn't enable to receive booking show a random image (before rush page) (more or less this page became the space near the door before the Blackfriday) (mantain it is a tradition)  
    |-laboratorioTime.html it's used to show the list of user booked to a preset laboratorioTime  
    |-studentTime.html it's used to show the list of booked lab to user  
    |-stampaLaboratori.php it's used to generate a list of all people booked in the laboratory (to improve but a starting base for a PDF generator)  
    |-css contain all the css of the structure
        |-bootstrap.min.css the current style is based on bootstrap with some customization  
        |-font-awesome.min.css this file became from font awesome icon library and help to show more friendly icon  
        |-SourceSansPro.css this file help to show a more cleaner font  
        |-Other *.css file other file are refered one by one to the html files present on the website folder and customize every page file to edit the style change here  
    |-js
        |-bootstrap.min.js it's used to enable beautiful effect became from the use of bootstrap component  
        |-Other *.js file is linked one by one with the html files present on the website folder and make the interaction with the API (The major of the modification reques is here)  
    |-api this contain all the call to api
        |-v1 this contain the API version 1
            |-AltoRouter.php this file translate from a call without a extension to the necessary redirect system (it's for improve the system secure)  
            |-index.php contain the list of all the api call and the neccesary route to call the executing class and function  
            |-settings.php contain a useful class to recovery and update .ini file that contain the settings  
            |-db.php contain all the db CRUD function and the system for connection with the db  
            |-config-www contain the all the configuration file require from the project  
                |-cogestione.ini contain the general and immutable settings as database settings  
                |-access.ini contain the mutable settings as the status of the booking  
            |-controller this contain all the class that contain the first response to API call it's are divide by category watch the index.php file to know where an API file is implemented  
                |-returnClass contain all the class that are useful to the API responder contained on the controller folder  
            |-algoritms contain some algoritms to implements in the future  
```  

NOTE: 
-----

The system don't support IE  